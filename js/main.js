(function ($) {
    $(document).ready(function () {

        let numberOfProducts;
        let loadLimit = 5;
        let limitBtn = false;
        let arr = [];
        let productsArr = [];


        $.getJSON('products.json', function(data){

            if (data.products) {
                getNumberProducts(data.products);
                if ($('body').hasClass('single-product-page')){

                    for (let i = 0; i < numberOfProducts; i++) {
                        productsArr.push(data.products[i]);
                    }
                    loadContentAboutProduct(data.products, loadInfo());
                } else {
                    for (let i = 0; i < numberOfProducts; i++) {
                        productsArr.push(data.products[i]);
                        if ((i < loadLimit)) {
                            createDOMforGrid(data.products[i]);

                        } else {
                            pastLoadMoreBtn();
                            arr.push(data.products[i]);
                        }
                    }
                }
            } else {
                console.log('Error! Incorrect JSON file.');
            }
            $('.slider').slick({
                slidesToShow: 1,
                slidesToScroll: 1
            });
        });


        function getNumberProducts(products){
            numberOfProducts = products.length;
        }


        function loadContentAboutProduct(products, id){
            let prodId = id - 1;
            if (prodId === null || prodId < 0) {
                prodId = 0;
            }

            $('body').attr('product-id', prodId);


            $('.single-product-title').text(products[prodId].title);
            $('.single-product-price').text('$'+products[prodId].price[0]);


            if (products[prodId].color) {
               let html = '';
                for (let i = 0; i < products[prodId].color.length; i++) {
                    let notAvColor = function () {
                        let arr = products[prodId].avaliable_color;
                        if(!(arr.indexOf(products[prodId].color[i]) != -1)){
                            return 'not-available'
                        } else {
                            return 'available'
                        }
                    };
                    html += '<div class="color ' + notAvColor() + '" style="width: 40px; height: 40px; background-color: '+ products[prodId].color[i] +'; background-image: url(' + 'images/materials/' + products[prodId].material + '-' + products[prodId].color[i] + '.jpg'+')">';
                    html += '<div class="popup-color"><span>'+ products[prodId].color[i] +' /4341</span><img alt="color" src="images/materials/'+ products[prodId].material + '-' + products[prodId].color[i] + '.jpg'+'"></div>';
                    html += '</div>';
                }
                $('.colors').empty().append(html);
                $('.colors').off('click');
                $('.color').on('click', function (event) {
                    changeColorforSinglePage(event);
                });

                $('.color').mouseenter(function () {
                    $(this).find('.popup-color').stop(true).fadeIn(300);
                });
                $('.color').mouseleave(function () {
                    $(this).find('.popup-color').stop(true).fadeOut(300);
                });
            }

            let sliderHTML ='';
            if (products[prodId].images) {

                for (let i = 0; i < products[prodId].images.length; i++){
                    sliderHTML += '<img src="images/'+ products[prodId].src +'/'+ products[prodId].images[i]+ '">';
                }


                if ($('.main-slider').hasClass('slick-initialized')){
                    $('.main-slider').slick('unslick');
                }
                if ($('.nav-slider').hasClass('slick-initialized')){
                    $('.nav-slider').slick('unslick');
                }
                $('.main-slider').empty().append(sliderHTML);
                $('.nav-slider').empty().append(sliderHTML);

                if ($('.main-slider').hasClass('slick-initialized')){
                    $('.main-slider').slick('unslick');
                }
                $('.main-slider').slick({
                    arrows: false,
                    draggable: false,
                    infinite: false
                });


                $('.nav-slider').slick({
                    infinite: false,
                    slidesToShow: 4,
                    slidesToScroll: 1,
                    asNavFor: '.main-slider',
                    focusOnSelect: true,
                    draggable: false,
                    arrows: true
                });
            }

            checkAvaliable();
        }


        function createDOMforGrid(products) {

            let html = '<div class="product-card" data-id="'+ products.id +'"><div class="product-card-inner">';
            if (products.images) {
                html += '<div class="product-card-images"><a class="product-card-link" href="product.html">';
                for (let i = 0; i < 2; i++){
                    if (products.images[i]){
                        html += '<img src="images/'+ products.src +'/'+ products.images[i]+ '">';
                    }
                }
                html += '</a></div>';
            }
            html += '<div class="product-card-footer">';
            html += '<div class="wrapper"><a class="product-card-link" href="product.html"><h3 class="product-card-title">' + products.title + '</h3></a>';
            html +='<div class="price-slider-'+ products.id +'">';
            for (let i = 0; i < products.price.length; i++) {
                html += '<span class="product-card-price animated">'+'$' + products.price[i] +'</span>';
            }

            html += '</div></div>';

            if (products.color) {
                html += '<div class="product-card-colors js-slider-color-' +products.id+ '">';
                for (let i = 0; i < products.color.length; i++) {
                    let notAvColor = function () {
                        let arr = products.avaliable_color;
                        if(!(arr.indexOf(products.color[i]) != -1)){
                            return 'not-available'
                        } else {
                            return 'available'
                        }
                    };
                    html += '<div class="product-card__color ' + notAvColor() + '" style="width: 40px; height: 40px; background-color: '+ products.color[i] +'; background-image: url(' + 'images/materials/' + products.material + '-' + products.color[i] + '.jpg'+')" "></div>';

                }
                html += '</div>';
            }
            html += '</div>';
            html += '</div></div>';

            let $lastEl = $('.product-card').last();

            let sliderClass = '.price-slider-' + products.id;
            let sliderColorClass = '.js-slider-color-' + products.id;

            $('.products-grid').append(html);
            $(sliderClass).slick({
                slidesToShow: 1,
                slidesToScroll: 1,
                draggable: false,
                swipe: false
            });

            $(sliderColorClass).slick({
                slidesToShow: 2,
                slidesToScroll: 1,
                asNavFor: sliderClass,
                focusOnSelect: true
            });

            $('.product-card-link').unbind('click');

            $('.product-card-link').on('click', function(event){
                saveInfo(event);
            });

            $lastEl.find('.product-card__color').on('click', function(event){
                changeColor(event)
            });

        }


        function changeColor(event) {

            let element = event.target.closest('.product-card');
            let productId = element.getAttribute('data-id');
            let colorPosition = $(event.target).index();
            changeContent(productId, colorPosition, element);
        }


        function changeColorforSinglePage(event) {
            let colorPosition = $(event.target).index();
            if (colorPosition === null) {
                colorPosition = 0;
            }

            $('body').attr('color-position', colorPosition);
            checkAvaliable();
        }


        function removeLoadMoreBtn() {
            $('.load-more').remove();
        }


        function saveInfo() {
            let element = event.target.closest('.product-card');
            let productId = element.getAttribute('data-id');
            if (element.hasAttribute('color-position')){
                let colorPos = element.getAttribute('color-position');
                localStorage.setItem('colorPos', colorPos);
            } else {
                localStorage.setItem('colorPos', 0);
            }
            localStorage.setItem('prodId', productId);
        }


        function loadInfo() {
            let prodId = localStorage.getItem('prodId');
            let colorPos = localStorage.getItem('colorPos');
            if (prodId === null) {
                prodId = 0;
            }

            if (colorPos === null) {
                colorPos = 0;
            }
            $('.next-product-btn').attr('data-switch-id', parseInt(prodId) + 1);
            $('body').attr('color-position', colorPos);
            if (prodId === 0 || prodId < 0) {
                $('.prev-product-btn').attr('data-switch-id', function () {
                    let prev = numberOfProducts;
                    return prev
                });
            } else {
                $('.prev-product-btn').attr('data-switch-id', prodId - 1)
            }
            localStorage.clear();

            return prodId
        }


        function changeContent(id, position, el){
            $(el).attr('color-position', position);
        }


        function pastLoadMoreBtn() {
            if (limitBtn == false) {
                let html = '<div class="product-card load-more">';
                html += 'Load more';
                html += '</div>';
                $('.products-grid').append(html);
                limitBtn = true;
                
                $('.load-more').on('click', function () {
                    loadNextContent();
                });
            }
        }


        function checkAvaliable() {
            let id = $('body').attr('product-id');
            let colorPos = $('body').attr('color-position');
            if (colorPos === null) {
                colorPos = 0;
            }

            if  (colorPos > productsArr[id].color.length - 1) {
                colorPos = 0;
                $('body').attr('color-position', colorPos);
            }

            for (let i = 0; i < productsArr[id].color.length; i++) {

                let currentColor = $('.color').eq(colorPos);
                $('.single-product-price').text('$'+productsArr[id].price[colorPos]);

                if(currentColor.hasClass('not-available')){
                   $('.single-product-cart').text('NOTIFY ME WHEN AVAILABLE');
                   $('.single-product-qty__field').attr('disabled', '').val(0);
                   $('.single-product-price').text('Wait list');
                   $('body').attr('product-available', 'false');
                } else {
                    $('.single-product-cart').text('Add to cart');
                    $('.single-product-qty__field').removeAttr('disabled').val(1);
                    $('body').attr('product-available', 'true');
                }
            }
        }


        function loadNextContent() {
            removeLoadMoreBtn();
            let addArr = [];
            for (let i = 0; i < arr.length; i++) {
                if (i < loadLimit){
                    createDOMforGrid(arr[i]);
                } else {
                    addArr.push(arr[i]);
                }

            }
            arr = addArr;
            if (arr.length > 0) {
                limitBtn = false;
                pastLoadMoreBtn();
            }
        }


        $('.btn-switch').on('click', function (event) {                         // Next/prev product switcher
            let el = event.target;
            loadContentAboutProduct(productsArr, $(el).attr('data-switch-id'));
            if ($(el).hasClass('next-product-btn')){
                let current = $(el).attr('data-switch-id');
                let prev = $('.prev-product-btn').attr('data-switch-id');
                current++;
                if(current > numberOfProducts) {
                    current = 0;
                }
                prev++;
                if (prev > numberOfProducts) {
                    prev = 0;
                }
                $(el).attr('data-switch-id', current);
                $('.prev-product-btn').attr('data-switch-id', prev);


            } else if ($(el).hasClass('prev-product-btn')){

                let current = $(el).attr('data-switch-id');
                let next = $('.next-product-btn').attr('data-switch-id');
                current--;
                if(current < 0) {
                    current = numberOfProducts;
                }
                next--;
                if (next < 0){
                    next = numberOfProducts;
                }
                $(el).attr('data-switch-id', current);
                $('.next-product-btn').attr('data-switch-id', next);
            }

        });


        $('.accordion-item__title').on('click', function(){

            $('.accordion-item__body').stop(true);
            if (!$(this).closest('.accordion-item').hasClass('is-active')) {
                $('.accordion-item').removeClass('is-active');
                $('.accordion-item__body').slideUp();
            }
            $(this).closest('.accordion-item').toggleClass('is-active');
            $(this).closest('.accordion-item').find('.accordion-item__body').slideToggle();

        });


        if($('.accordion-item').length){
            setTimeout(function(){
                $('.accordion-item').first().find('.accordion-item__title').click();
            },100);
        }


        $('.single-product-qty__field').on('change', function () {
            let val = $('.single-product-qty__field').val();
            if (val > 99) {
                val = 99;
                $('.single-product-qty__field').val(val);
            } else if (val < 1) {
                val = 1;
                $('.single-product-qty__field').val(val);
            }
        });


        $('.single-product-cart').on('click',function(){
            let currentAttr = $('body').attr('product-available');
            if (currentAttr == 'true') {
                let popup = '<div class="popup"><ul><li>Congratulations!</li><li>'+ $('.single-product-title').text() +'</li><li>was added</li><li>to your cart!</li></ul><button class="cart-close-button">Close</button></div>';
                $('body').append(popup);
                $('.popup').hide().fadeIn(300);
            } else {
                let popup = '<div class="popup"><ul><li>Sorry!</li><li>'+ $('.single-product-title').text() +'</li><li>with this options</li><li>is not available now.</li><li>We notify your when it available again.</li></ul><button class="cart-close-button">Close</button></div>';
                $('body').append(popup);
                $('.popup').hide().fadeIn(300);
            }

            $('.cart-close-button').on('click', function(){
                 $('.popup').fadeOut(300).remove();
            });

            $('body').on('click', function (event) {
                if (!$(event.target).closest($('.popup').add('.single-product-cart')).length) {
                    $('.popup').fadeOut(300).remove();
                }
            })
        });

    });
})(jQuery);