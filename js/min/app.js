(function(){function r(e,n,t){function o(i,f){if(!n[i]){if(!e[i]){var c="function"==typeof require&&require;if(!f&&c)return c(i,!0);if(u)return u(i,!0);var a=new Error("Cannot find module '"+i+"'");throw a.code="MODULE_NOT_FOUND",a}var p=n[i]={exports:{}};e[i][0].call(p.exports,function(r){var n=e[i][1][r];return o(n||r)},p,p.exports,r,e,n,t)}return n[i].exports}for(var u="function"==typeof require&&require,i=0;i<t.length;i++)o(t[i]);return o}return r})()({1:[function(require,module,exports){
'use strict';

(function ($) {
    $(document).ready(function () {

        var numberOfProducts = void 0;
        var loadLimit = 5;
        var limitBtn = false;
        var arr = [];
        var productsArr = [];

        $.getJSON('products.json', function (data) {

            if (data.products) {
                getNumberProducts(data.products);
                if ($('body').hasClass('single-product-page')) {

                    for (var i = 0; i < numberOfProducts; i++) {
                        productsArr.push(data.products[i]);
                    }
                    loadContentAboutProduct(data.products, loadInfo());
                } else {
                    for (var _i = 0; _i < numberOfProducts; _i++) {
                        productsArr.push(data.products[_i]);
                        if (_i < loadLimit) {
                            createDOMforGrid(data.products[_i]);
                        } else {
                            pastLoadMoreBtn();
                            arr.push(data.products[_i]);
                        }
                    }
                }
            } else {
                console.log('Error! Incorrect JSON file.');
            }
            $('.slider').slick({
                slidesToShow: 1,
                slidesToScroll: 1
            });
        });

        function getNumberProducts(products) {
            numberOfProducts = products.length;
        }

        function loadContentAboutProduct(products, id) {
            var prodId = id - 1;
            if (prodId === null || prodId < 0) {
                prodId = 0;
            }

            $('body').attr('product-id', prodId);

            $('.single-product-title').text(products[prodId].title);
            $('.single-product-price').text('$' + products[prodId].price[0]);

            if (products[prodId].color) {
                var html = '';

                var _loop = function _loop(i) {
                    var notAvColor = function notAvColor() {
                        var arr = products[prodId].avaliable_color;
                        if (!(arr.indexOf(products[prodId].color[i]) != -1)) {
                            return 'not-available';
                        } else {
                            return 'available';
                        }
                    };
                    html += '<div class="color ' + notAvColor() + '" style="width: 40px; height: 40px; background-color: ' + products[prodId].color[i] + '; background-image: url(' + 'images/materials/' + products[prodId].material + '-' + products[prodId].color[i] + '.jpg' + ')">';
                    html += '<div class="popup-color"><span>' + products[prodId].color[i] + ' /4341</span><img alt="color" src="images/materials/' + products[prodId].material + '-' + products[prodId].color[i] + '.jpg' + '"></div>';
                    html += '</div>';
                };

                for (var i = 0; i < products[prodId].color.length; i++) {
                    _loop(i);
                }
                $('.colors').empty().append(html);
                $('.colors').off('click');
                $('.color').on('click', function (event) {
                    changeColorforSinglePage(event);
                });

                $('.color').mouseenter(function () {
                    $(this).find('.popup-color').stop(true).fadeIn(300);
                });
                $('.color').mouseleave(function () {
                    $(this).find('.popup-color').stop(true).fadeOut(300);
                });
            }

            var sliderHTML = '';
            if (products[prodId].images) {

                for (var i = 0; i < products[prodId].images.length; i++) {
                    sliderHTML += '<img src="images/' + products[prodId].src + '/' + products[prodId].images[i] + '">';
                }

                if ($('.main-slider').hasClass('slick-initialized')) {
                    $('.main-slider').slick('unslick');
                }
                if ($('.nav-slider').hasClass('slick-initialized')) {
                    $('.nav-slider').slick('unslick');
                }
                $('.main-slider').empty().append(sliderHTML);
                $('.nav-slider').empty().append(sliderHTML);

                if ($('.main-slider').hasClass('slick-initialized')) {
                    $('.main-slider').slick('unslick');
                }
                $('.main-slider').slick({
                    arrows: false,
                    draggable: false,
                    infinite: false
                });

                $('.nav-slider').slick({
                    infinite: false,
                    slidesToShow: 4,
                    slidesToScroll: 1,
                    asNavFor: '.main-slider',
                    focusOnSelect: true,
                    draggable: false,
                    arrows: true
                });
            }

            checkAvaliable();
        }

        function createDOMforGrid(products) {

            var html = '<div class="product-card" data-id="' + products.id + '"><div class="product-card-inner">';
            if (products.images) {
                html += '<div class="product-card-images"><a class="product-card-link" href="product.html">';
                for (var i = 0; i < 2; i++) {
                    if (products.images[i]) {
                        html += '<img src="images/' + products.src + '/' + products.images[i] + '">';
                    }
                }
                html += '</a></div>';
            }
            html += '<div class="product-card-footer">';
            html += '<div class="wrapper"><a class="product-card-link" href="product.html"><h3 class="product-card-title">' + products.title + '</h3></a>';
            html += '<div class="price-slider-' + products.id + '">';
            for (var _i2 = 0; _i2 < products.price.length; _i2++) {
                html += '<span class="product-card-price animated">' + '$' + products.price[_i2] + '</span>';
            }

            html += '</div></div>';

            if (products.color) {
                html += '<div class="product-card-colors js-slider-color-' + products.id + '">';

                var _loop2 = function _loop2(_i3) {
                    var notAvColor = function notAvColor() {
                        var arr = products.avaliable_color;
                        if (!(arr.indexOf(products.color[_i3]) != -1)) {
                            return 'not-available';
                        } else {
                            return 'available';
                        }
                    };
                    html += '<div class="product-card__color ' + notAvColor() + '" style="width: 40px; height: 40px; background-color: ' + products.color[_i3] + '; background-image: url(' + 'images/materials/' + products.material + '-' + products.color[_i3] + '.jpg' + ')" "></div>';
                };

                for (var _i3 = 0; _i3 < products.color.length; _i3++) {
                    _loop2(_i3);
                }
                html += '</div>';
            }
            html += '</div>';
            html += '</div></div>';

            var $lastEl = $('.product-card').last();

            var sliderClass = '.price-slider-' + products.id;
            var sliderColorClass = '.js-slider-color-' + products.id;

            $('.products-grid').append(html);
            $(sliderClass).slick({
                slidesToShow: 1,
                slidesToScroll: 1,
                draggable: false,
                swipe: false
            });

            $(sliderColorClass).slick({
                slidesToShow: 2,
                slidesToScroll: 1,
                asNavFor: sliderClass,
                focusOnSelect: true
            });

            $('.product-card-link').unbind('click');

            $('.product-card-link').on('click', function (event) {
                saveInfo(event);
            });

            $lastEl.find('.product-card__color').on('click', function (event) {
                changeColor(event);
            });
        }

        function changeColor(event) {

            var element = event.target.closest('.product-card');
            var productId = element.getAttribute('data-id');
            var colorPosition = $(event.target).index();
            changeContent(productId, colorPosition, element);
        }

        function changeColorforSinglePage(event) {
            var colorPosition = $(event.target).index();
            if (colorPosition === null) {
                colorPosition = 0;
            }

            $('body').attr('color-position', colorPosition);
            checkAvaliable();
        }

        function removeLoadMoreBtn() {
            $('.load-more').remove();
        }

        function saveInfo() {
            var element = event.target.closest('.product-card');
            var productId = element.getAttribute('data-id');
            if (element.hasAttribute('color-position')) {
                var colorPos = element.getAttribute('color-position');
                localStorage.setItem('colorPos', colorPos);
            } else {
                localStorage.setItem('colorPos', 0);
            }
            localStorage.setItem('prodId', productId);
        }

        function loadInfo() {
            var prodId = localStorage.getItem('prodId');
            var colorPos = localStorage.getItem('colorPos');
            if (prodId === null) {
                prodId = 0;
            }

            if (colorPos === null) {
                colorPos = 0;
            }
            $('.next-product-btn').attr('data-switch-id', parseInt(prodId) + 1);
            $('body').attr('color-position', colorPos);
            if (prodId === 0 || prodId < 0) {
                $('.prev-product-btn').attr('data-switch-id', function () {
                    var prev = numberOfProducts;
                    return prev;
                });
            } else {
                $('.prev-product-btn').attr('data-switch-id', prodId - 1);
            }
            localStorage.clear();

            return prodId;
        }

        function changeContent(id, position, el) {
            $(el).attr('color-position', position);
        }

        function pastLoadMoreBtn() {
            if (limitBtn == false) {
                var html = '<div class="product-card load-more">';
                html += 'Load more';
                html += '</div>';
                $('.products-grid').append(html);
                limitBtn = true;

                $('.load-more').on('click', function () {
                    loadNextContent();
                });
            }
        }

        function checkAvaliable() {
            var id = $('body').attr('product-id');
            var colorPos = $('body').attr('color-position');
            if (colorPos === null) {
                colorPos = 0;
            }

            if (colorPos > productsArr[id].color.length - 1) {
                colorPos = 0;
                $('body').attr('color-position', colorPos);
            }

            for (var i = 0; i < productsArr[id].color.length; i++) {

                var currentColor = $('.color').eq(colorPos);
                $('.single-product-price').text('$' + productsArr[id].price[colorPos]);

                if (currentColor.hasClass('not-available')) {
                    $('.single-product-cart').text('NOTIFY ME WHEN AVAILABLE');
                    $('.single-product-qty__field').attr('disabled', '').val(0);
                    $('.single-product-price').text('Wait list');
                    $('body').attr('product-available', 'false');
                } else {
                    $('.single-product-cart').text('Add to cart');
                    $('.single-product-qty__field').removeAttr('disabled').val(1);
                    $('body').attr('product-available', 'true');
                }
            }
        }

        function loadNextContent() {
            removeLoadMoreBtn();
            var addArr = [];
            for (var i = 0; i < arr.length; i++) {
                if (i < loadLimit) {
                    createDOMforGrid(arr[i]);
                } else {
                    addArr.push(arr[i]);
                }
            }
            arr = addArr;
            if (arr.length > 0) {
                limitBtn = false;
                pastLoadMoreBtn();
            }
        }

        $('.btn-switch').on('click', function (event) {
            // Next/prev product switcher
            var el = event.target;
            loadContentAboutProduct(productsArr, $(el).attr('data-switch-id'));
            if ($(el).hasClass('next-product-btn')) {
                var current = $(el).attr('data-switch-id');
                var prev = $('.prev-product-btn').attr('data-switch-id');
                current++;
                if (current > numberOfProducts) {
                    current = 0;
                }
                prev++;
                if (prev > numberOfProducts) {
                    prev = 0;
                }
                $(el).attr('data-switch-id', current);
                $('.prev-product-btn').attr('data-switch-id', prev);
            } else if ($(el).hasClass('prev-product-btn')) {

                var _current = $(el).attr('data-switch-id');
                var next = $('.next-product-btn').attr('data-switch-id');
                _current--;
                if (_current < 0) {
                    _current = numberOfProducts;
                }
                next--;
                if (next < 0) {
                    next = numberOfProducts;
                }
                $(el).attr('data-switch-id', _current);
                $('.next-product-btn').attr('data-switch-id', next);
            }
        });

        $('.accordion-item__title').on('click', function () {

            $('.accordion-item__body').stop(true);
            if (!$(this).closest('.accordion-item').hasClass('is-active')) {
                $('.accordion-item').removeClass('is-active');
                $('.accordion-item__body').slideUp();
            }
            $(this).closest('.accordion-item').toggleClass('is-active');
            $(this).closest('.accordion-item').find('.accordion-item__body').slideToggle();
        });

        if ($('.accordion-item').length) {
            setTimeout(function () {
                $('.accordion-item').first().find('.accordion-item__title').click();
            }, 100);
        }

        $('.single-product-qty__field').on('change', function () {
            var val = $('.single-product-qty__field').val();
            if (val > 99) {
                val = 99;
                $('.single-product-qty__field').val(val);
            } else if (val < 1) {
                val = 1;
                $('.single-product-qty__field').val(val);
            }
        });

        $('.single-product-cart').on('click', function () {
            var currentAttr = $('body').attr('product-available');
            if (currentAttr == 'true') {
                var popup = '<div class="popup"><ul><li>Congratulations!</li><li>' + $('.single-product-title').text() + '</li><li>was added</li><li>to your cart!</li></ul><button class="cart-close-button">Close</button></div>';
                $('body').append(popup);
                $('.popup').hide().fadeIn(300);
            } else {
                var _popup = '<div class="popup"><ul><li>Sorry!</li><li>' + $('.single-product-title').text() + '</li><li>with this options</li><li>is not available now.</li><li>We notify your when it available again.</li></ul><button class="cart-close-button">Close</button></div>';
                $('body').append(_popup);
                $('.popup').hide().fadeIn(300);
            }

            $('.cart-close-button').on('click', function () {
                $('.popup').fadeOut(300).remove();
            });

            $('body').on('click', function (event) {
                if (!$(event.target).closest($('.popup').add('.single-product-cart')).length) {
                    $('.popup').fadeOut(300).remove();
                }
            });
        });
    });
})(jQuery);

},{}]},{},[1])
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm5vZGVfbW9kdWxlcy9icm93c2VyLXBhY2svX3ByZWx1ZGUuanMiLCJqcy9tYWluLmpzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBOzs7QUNBQSxDQUFDLFVBQVUsQ0FBVixFQUFhO0FBQ1YsTUFBRSxRQUFGLEVBQVksS0FBWixDQUFrQixZQUFZOztBQUUxQixZQUFJLHlCQUFKO0FBQ0EsWUFBSSxZQUFZLENBQWhCO0FBQ0EsWUFBSSxXQUFXLEtBQWY7QUFDQSxZQUFJLE1BQU0sRUFBVjtBQUNBLFlBQUksY0FBYyxFQUFsQjs7QUFHQSxVQUFFLE9BQUYsQ0FBVSxlQUFWLEVBQTJCLFVBQVMsSUFBVCxFQUFjOztBQUVyQyxnQkFBSSxLQUFLLFFBQVQsRUFBbUI7QUFDZixrQ0FBa0IsS0FBSyxRQUF2QjtBQUNBLG9CQUFJLEVBQUUsTUFBRixFQUFVLFFBQVYsQ0FBbUIscUJBQW5CLENBQUosRUFBOEM7O0FBRTFDLHlCQUFLLElBQUksSUFBSSxDQUFiLEVBQWdCLElBQUksZ0JBQXBCLEVBQXNDLEdBQXRDLEVBQTJDO0FBQ3ZDLG9DQUFZLElBQVosQ0FBaUIsS0FBSyxRQUFMLENBQWMsQ0FBZCxDQUFqQjtBQUNIO0FBQ0QsNENBQXdCLEtBQUssUUFBN0IsRUFBdUMsVUFBdkM7QUFDSCxpQkFORCxNQU1PO0FBQ0gseUJBQUssSUFBSSxLQUFJLENBQWIsRUFBZ0IsS0FBSSxnQkFBcEIsRUFBc0MsSUFBdEMsRUFBMkM7QUFDdkMsb0NBQVksSUFBWixDQUFpQixLQUFLLFFBQUwsQ0FBYyxFQUFkLENBQWpCO0FBQ0EsNEJBQUssS0FBSSxTQUFULEVBQXFCO0FBQ2pCLDZDQUFpQixLQUFLLFFBQUwsQ0FBYyxFQUFkLENBQWpCO0FBRUgseUJBSEQsTUFHTztBQUNIO0FBQ0EsZ0NBQUksSUFBSixDQUFTLEtBQUssUUFBTCxDQUFjLEVBQWQsQ0FBVDtBQUNIO0FBQ0o7QUFDSjtBQUNKLGFBcEJELE1Bb0JPO0FBQ0gsd0JBQVEsR0FBUixDQUFZLDZCQUFaO0FBQ0g7QUFDRCxjQUFFLFNBQUYsRUFBYSxLQUFiLENBQW1CO0FBQ2YsOEJBQWMsQ0FEQztBQUVmLGdDQUFnQjtBQUZELGFBQW5CO0FBSUgsU0E3QkQ7O0FBZ0NBLGlCQUFTLGlCQUFULENBQTJCLFFBQTNCLEVBQW9DO0FBQ2hDLCtCQUFtQixTQUFTLE1BQTVCO0FBQ0g7O0FBR0QsaUJBQVMsdUJBQVQsQ0FBaUMsUUFBakMsRUFBMkMsRUFBM0MsRUFBOEM7QUFDMUMsZ0JBQUksU0FBUyxLQUFLLENBQWxCO0FBQ0EsZ0JBQUksV0FBVyxJQUFYLElBQW1CLFNBQVMsQ0FBaEMsRUFBbUM7QUFDL0IseUJBQVMsQ0FBVDtBQUNIOztBQUVELGNBQUUsTUFBRixFQUFVLElBQVYsQ0FBZSxZQUFmLEVBQTZCLE1BQTdCOztBQUdBLGNBQUUsdUJBQUYsRUFBMkIsSUFBM0IsQ0FBZ0MsU0FBUyxNQUFULEVBQWlCLEtBQWpEO0FBQ0EsY0FBRSx1QkFBRixFQUEyQixJQUEzQixDQUFnQyxNQUFJLFNBQVMsTUFBVCxFQUFpQixLQUFqQixDQUF1QixDQUF2QixDQUFwQzs7QUFHQSxnQkFBSSxTQUFTLE1BQVQsRUFBaUIsS0FBckIsRUFBNEI7QUFDekIsb0JBQUksT0FBTyxFQUFYOztBQUR5QiwyQ0FFZixDQUZlO0FBR3BCLHdCQUFJLGFBQWEsU0FBYixVQUFhLEdBQVk7QUFDekIsNEJBQUksTUFBTSxTQUFTLE1BQVQsRUFBaUIsZUFBM0I7QUFDQSw0QkFBRyxFQUFFLElBQUksT0FBSixDQUFZLFNBQVMsTUFBVCxFQUFpQixLQUFqQixDQUF1QixDQUF2QixDQUFaLEtBQTBDLENBQUMsQ0FBN0MsQ0FBSCxFQUFtRDtBQUMvQyxtQ0FBTyxlQUFQO0FBQ0gseUJBRkQsTUFFTztBQUNILG1DQUFPLFdBQVA7QUFDSDtBQUNKLHFCQVBEO0FBUUEsNEJBQVEsdUJBQXVCLFlBQXZCLEdBQXNDLHdEQUF0QyxHQUFnRyxTQUFTLE1BQVQsRUFBaUIsS0FBakIsQ0FBdUIsQ0FBdkIsQ0FBaEcsR0FBMkgsMEJBQTNILEdBQXdKLG1CQUF4SixHQUE4SyxTQUFTLE1BQVQsRUFBaUIsUUFBL0wsR0FBME0sR0FBMU0sR0FBZ04sU0FBUyxNQUFULEVBQWlCLEtBQWpCLENBQXVCLENBQXZCLENBQWhOLEdBQTRPLE1BQTVPLEdBQW1QLEtBQTNQO0FBQ0EsNEJBQVEsb0NBQW1DLFNBQVMsTUFBVCxFQUFpQixLQUFqQixDQUF1QixDQUF2QixDQUFuQyxHQUE4RCxzREFBOUQsR0FBc0gsU0FBUyxNQUFULEVBQWlCLFFBQXZJLEdBQWtKLEdBQWxKLEdBQXdKLFNBQVMsTUFBVCxFQUFpQixLQUFqQixDQUF1QixDQUF2QixDQUF4SixHQUFvTCxNQUFwTCxHQUEyTCxVQUFuTTtBQUNBLDRCQUFRLFFBQVI7QUFib0I7O0FBRXhCLHFCQUFLLElBQUksSUFBSSxDQUFiLEVBQWdCLElBQUksU0FBUyxNQUFULEVBQWlCLEtBQWpCLENBQXVCLE1BQTNDLEVBQW1ELEdBQW5ELEVBQXdEO0FBQUEsMEJBQS9DLENBQStDO0FBWXZEO0FBQ0Qsa0JBQUUsU0FBRixFQUFhLEtBQWIsR0FBcUIsTUFBckIsQ0FBNEIsSUFBNUI7QUFDQSxrQkFBRSxTQUFGLEVBQWEsR0FBYixDQUFpQixPQUFqQjtBQUNBLGtCQUFFLFFBQUYsRUFBWSxFQUFaLENBQWUsT0FBZixFQUF3QixVQUFVLEtBQVYsRUFBaUI7QUFDckMsNkNBQXlCLEtBQXpCO0FBQ0gsaUJBRkQ7O0FBSUEsa0JBQUUsUUFBRixFQUFZLFVBQVosQ0FBdUIsWUFBWTtBQUMvQixzQkFBRSxJQUFGLEVBQVEsSUFBUixDQUFhLGNBQWIsRUFBNkIsSUFBN0IsQ0FBa0MsSUFBbEMsRUFBd0MsTUFBeEMsQ0FBK0MsR0FBL0M7QUFDSCxpQkFGRDtBQUdBLGtCQUFFLFFBQUYsRUFBWSxVQUFaLENBQXVCLFlBQVk7QUFDL0Isc0JBQUUsSUFBRixFQUFRLElBQVIsQ0FBYSxjQUFiLEVBQTZCLElBQTdCLENBQWtDLElBQWxDLEVBQXdDLE9BQXhDLENBQWdELEdBQWhEO0FBQ0gsaUJBRkQ7QUFHSDs7QUFFRCxnQkFBSSxhQUFZLEVBQWhCO0FBQ0EsZ0JBQUksU0FBUyxNQUFULEVBQWlCLE1BQXJCLEVBQTZCOztBQUV6QixxQkFBSyxJQUFJLElBQUksQ0FBYixFQUFnQixJQUFJLFNBQVMsTUFBVCxFQUFpQixNQUFqQixDQUF3QixNQUE1QyxFQUFvRCxHQUFwRCxFQUF3RDtBQUNwRCxrQ0FBYyxzQkFBcUIsU0FBUyxNQUFULEVBQWlCLEdBQXRDLEdBQTJDLEdBQTNDLEdBQWdELFNBQVMsTUFBVCxFQUFpQixNQUFqQixDQUF3QixDQUF4QixDQUFoRCxHQUE0RSxJQUExRjtBQUNIOztBQUdELG9CQUFJLEVBQUUsY0FBRixFQUFrQixRQUFsQixDQUEyQixtQkFBM0IsQ0FBSixFQUFvRDtBQUNoRCxzQkFBRSxjQUFGLEVBQWtCLEtBQWxCLENBQXdCLFNBQXhCO0FBQ0g7QUFDRCxvQkFBSSxFQUFFLGFBQUYsRUFBaUIsUUFBakIsQ0FBMEIsbUJBQTFCLENBQUosRUFBbUQ7QUFDL0Msc0JBQUUsYUFBRixFQUFpQixLQUFqQixDQUF1QixTQUF2QjtBQUNIO0FBQ0Qsa0JBQUUsY0FBRixFQUFrQixLQUFsQixHQUEwQixNQUExQixDQUFpQyxVQUFqQztBQUNBLGtCQUFFLGFBQUYsRUFBaUIsS0FBakIsR0FBeUIsTUFBekIsQ0FBZ0MsVUFBaEM7O0FBRUEsb0JBQUksRUFBRSxjQUFGLEVBQWtCLFFBQWxCLENBQTJCLG1CQUEzQixDQUFKLEVBQW9EO0FBQ2hELHNCQUFFLGNBQUYsRUFBa0IsS0FBbEIsQ0FBd0IsU0FBeEI7QUFDSDtBQUNELGtCQUFFLGNBQUYsRUFBa0IsS0FBbEIsQ0FBd0I7QUFDcEIsNEJBQVEsS0FEWTtBQUVwQiwrQkFBVyxLQUZTO0FBR3BCLDhCQUFVO0FBSFUsaUJBQXhCOztBQU9BLGtCQUFFLGFBQUYsRUFBaUIsS0FBakIsQ0FBdUI7QUFDbkIsOEJBQVUsS0FEUztBQUVuQixrQ0FBYyxDQUZLO0FBR25CLG9DQUFnQixDQUhHO0FBSW5CLDhCQUFVLGNBSlM7QUFLbkIsbUNBQWUsSUFMSTtBQU1uQiwrQkFBVyxLQU5RO0FBT25CLDRCQUFRO0FBUFcsaUJBQXZCO0FBU0g7O0FBRUQ7QUFDSDs7QUFHRCxpQkFBUyxnQkFBVCxDQUEwQixRQUExQixFQUFvQzs7QUFFaEMsZ0JBQUksT0FBTyx3Q0FBdUMsU0FBUyxFQUFoRCxHQUFvRCxvQ0FBL0Q7QUFDQSxnQkFBSSxTQUFTLE1BQWIsRUFBcUI7QUFDakIsd0JBQVEsb0ZBQVI7QUFDQSxxQkFBSyxJQUFJLElBQUksQ0FBYixFQUFnQixJQUFJLENBQXBCLEVBQXVCLEdBQXZCLEVBQTJCO0FBQ3ZCLHdCQUFJLFNBQVMsTUFBVCxDQUFnQixDQUFoQixDQUFKLEVBQXVCO0FBQ25CLGdDQUFRLHNCQUFxQixTQUFTLEdBQTlCLEdBQW1DLEdBQW5DLEdBQXdDLFNBQVMsTUFBVCxDQUFnQixDQUFoQixDQUF4QyxHQUE0RCxJQUFwRTtBQUNIO0FBQ0o7QUFDRCx3QkFBUSxZQUFSO0FBQ0g7QUFDRCxvQkFBUSxtQ0FBUjtBQUNBLG9CQUFRLDBHQUEwRyxTQUFTLEtBQW5ILEdBQTJILFdBQW5JO0FBQ0Esb0JBQU8sOEJBQTZCLFNBQVMsRUFBdEMsR0FBMEMsSUFBakQ7QUFDQSxpQkFBSyxJQUFJLE1BQUksQ0FBYixFQUFnQixNQUFJLFNBQVMsS0FBVCxDQUFlLE1BQW5DLEVBQTJDLEtBQTNDLEVBQWdEO0FBQzVDLHdCQUFRLCtDQUE2QyxHQUE3QyxHQUFtRCxTQUFTLEtBQVQsQ0FBZSxHQUFmLENBQW5ELEdBQXNFLFNBQTlFO0FBQ0g7O0FBRUQsb0JBQVEsY0FBUjs7QUFFQSxnQkFBSSxTQUFTLEtBQWIsRUFBb0I7QUFDaEIsd0JBQVEscURBQW9ELFNBQVMsRUFBN0QsR0FBaUUsSUFBekU7O0FBRGdCLDZDQUVQLEdBRk87QUFHWix3QkFBSSxhQUFhLFNBQWIsVUFBYSxHQUFZO0FBQ3pCLDRCQUFJLE1BQU0sU0FBUyxlQUFuQjtBQUNBLDRCQUFHLEVBQUUsSUFBSSxPQUFKLENBQVksU0FBUyxLQUFULENBQWUsR0FBZixDQUFaLEtBQWtDLENBQUMsQ0FBckMsQ0FBSCxFQUEyQztBQUN2QyxtQ0FBTyxlQUFQO0FBQ0gseUJBRkQsTUFFTztBQUNILG1DQUFPLFdBQVA7QUFDSDtBQUNKLHFCQVBEO0FBUUEsNEJBQVEscUNBQXFDLFlBQXJDLEdBQW9ELHdEQUFwRCxHQUE4RyxTQUFTLEtBQVQsQ0FBZSxHQUFmLENBQTlHLEdBQWlJLDBCQUFqSSxHQUE4SixtQkFBOUosR0FBb0wsU0FBUyxRQUE3TCxHQUF3TSxHQUF4TSxHQUE4TSxTQUFTLEtBQVQsQ0FBZSxHQUFmLENBQTlNLEdBQWtPLE1BQWxPLEdBQXlPLGFBQWpQO0FBWFk7O0FBRWhCLHFCQUFLLElBQUksTUFBSSxDQUFiLEVBQWdCLE1BQUksU0FBUyxLQUFULENBQWUsTUFBbkMsRUFBMkMsS0FBM0MsRUFBZ0Q7QUFBQSwyQkFBdkMsR0FBdUM7QUFXL0M7QUFDRCx3QkFBUSxRQUFSO0FBQ0g7QUFDRCxvQkFBUSxRQUFSO0FBQ0Esb0JBQVEsY0FBUjs7QUFFQSxnQkFBSSxVQUFVLEVBQUUsZUFBRixFQUFtQixJQUFuQixFQUFkOztBQUVBLGdCQUFJLGNBQWMsbUJBQW1CLFNBQVMsRUFBOUM7QUFDQSxnQkFBSSxtQkFBbUIsc0JBQXNCLFNBQVMsRUFBdEQ7O0FBRUEsY0FBRSxnQkFBRixFQUFvQixNQUFwQixDQUEyQixJQUEzQjtBQUNBLGNBQUUsV0FBRixFQUFlLEtBQWYsQ0FBcUI7QUFDakIsOEJBQWMsQ0FERztBQUVqQixnQ0FBZ0IsQ0FGQztBQUdqQiwyQkFBVyxLQUhNO0FBSWpCLHVCQUFPO0FBSlUsYUFBckI7O0FBT0EsY0FBRSxnQkFBRixFQUFvQixLQUFwQixDQUEwQjtBQUN0Qiw4QkFBYyxDQURRO0FBRXRCLGdDQUFnQixDQUZNO0FBR3RCLDBCQUFVLFdBSFk7QUFJdEIsK0JBQWU7QUFKTyxhQUExQjs7QUFPQSxjQUFFLG9CQUFGLEVBQXdCLE1BQXhCLENBQStCLE9BQS9COztBQUVBLGNBQUUsb0JBQUYsRUFBd0IsRUFBeEIsQ0FBMkIsT0FBM0IsRUFBb0MsVUFBUyxLQUFULEVBQWU7QUFDL0MseUJBQVMsS0FBVDtBQUNILGFBRkQ7O0FBSUEsb0JBQVEsSUFBUixDQUFhLHNCQUFiLEVBQXFDLEVBQXJDLENBQXdDLE9BQXhDLEVBQWlELFVBQVMsS0FBVCxFQUFlO0FBQzVELDRCQUFZLEtBQVo7QUFDSCxhQUZEO0FBSUg7O0FBR0QsaUJBQVMsV0FBVCxDQUFxQixLQUFyQixFQUE0Qjs7QUFFeEIsZ0JBQUksVUFBVSxNQUFNLE1BQU4sQ0FBYSxPQUFiLENBQXFCLGVBQXJCLENBQWQ7QUFDQSxnQkFBSSxZQUFZLFFBQVEsWUFBUixDQUFxQixTQUFyQixDQUFoQjtBQUNBLGdCQUFJLGdCQUFnQixFQUFFLE1BQU0sTUFBUixFQUFnQixLQUFoQixFQUFwQjtBQUNBLDBCQUFjLFNBQWQsRUFBeUIsYUFBekIsRUFBd0MsT0FBeEM7QUFDSDs7QUFHRCxpQkFBUyx3QkFBVCxDQUFrQyxLQUFsQyxFQUF5QztBQUNyQyxnQkFBSSxnQkFBZ0IsRUFBRSxNQUFNLE1BQVIsRUFBZ0IsS0FBaEIsRUFBcEI7QUFDQSxnQkFBSSxrQkFBa0IsSUFBdEIsRUFBNEI7QUFDeEIsZ0NBQWdCLENBQWhCO0FBQ0g7O0FBRUQsY0FBRSxNQUFGLEVBQVUsSUFBVixDQUFlLGdCQUFmLEVBQWlDLGFBQWpDO0FBQ0E7QUFDSDs7QUFHRCxpQkFBUyxpQkFBVCxHQUE2QjtBQUN6QixjQUFFLFlBQUYsRUFBZ0IsTUFBaEI7QUFDSDs7QUFHRCxpQkFBUyxRQUFULEdBQW9CO0FBQ2hCLGdCQUFJLFVBQVUsTUFBTSxNQUFOLENBQWEsT0FBYixDQUFxQixlQUFyQixDQUFkO0FBQ0EsZ0JBQUksWUFBWSxRQUFRLFlBQVIsQ0FBcUIsU0FBckIsQ0FBaEI7QUFDQSxnQkFBSSxRQUFRLFlBQVIsQ0FBcUIsZ0JBQXJCLENBQUosRUFBMkM7QUFDdkMsb0JBQUksV0FBVyxRQUFRLFlBQVIsQ0FBcUIsZ0JBQXJCLENBQWY7QUFDQSw2QkFBYSxPQUFiLENBQXFCLFVBQXJCLEVBQWlDLFFBQWpDO0FBQ0gsYUFIRCxNQUdPO0FBQ0gsNkJBQWEsT0FBYixDQUFxQixVQUFyQixFQUFpQyxDQUFqQztBQUNIO0FBQ0QseUJBQWEsT0FBYixDQUFxQixRQUFyQixFQUErQixTQUEvQjtBQUNIOztBQUdELGlCQUFTLFFBQVQsR0FBb0I7QUFDaEIsZ0JBQUksU0FBUyxhQUFhLE9BQWIsQ0FBcUIsUUFBckIsQ0FBYjtBQUNBLGdCQUFJLFdBQVcsYUFBYSxPQUFiLENBQXFCLFVBQXJCLENBQWY7QUFDQSxnQkFBSSxXQUFXLElBQWYsRUFBcUI7QUFDakIseUJBQVMsQ0FBVDtBQUNIOztBQUVELGdCQUFJLGFBQWEsSUFBakIsRUFBdUI7QUFDbkIsMkJBQVcsQ0FBWDtBQUNIO0FBQ0QsY0FBRSxtQkFBRixFQUF1QixJQUF2QixDQUE0QixnQkFBNUIsRUFBOEMsU0FBUyxNQUFULElBQW1CLENBQWpFO0FBQ0EsY0FBRSxNQUFGLEVBQVUsSUFBVixDQUFlLGdCQUFmLEVBQWlDLFFBQWpDO0FBQ0EsZ0JBQUksV0FBVyxDQUFYLElBQWdCLFNBQVMsQ0FBN0IsRUFBZ0M7QUFDNUIsa0JBQUUsbUJBQUYsRUFBdUIsSUFBdkIsQ0FBNEIsZ0JBQTVCLEVBQThDLFlBQVk7QUFDdEQsd0JBQUksT0FBTyxnQkFBWDtBQUNBLDJCQUFPLElBQVA7QUFDSCxpQkFIRDtBQUlILGFBTEQsTUFLTztBQUNILGtCQUFFLG1CQUFGLEVBQXVCLElBQXZCLENBQTRCLGdCQUE1QixFQUE4QyxTQUFTLENBQXZEO0FBQ0g7QUFDRCx5QkFBYSxLQUFiOztBQUVBLG1CQUFPLE1BQVA7QUFDSDs7QUFHRCxpQkFBUyxhQUFULENBQXVCLEVBQXZCLEVBQTJCLFFBQTNCLEVBQXFDLEVBQXJDLEVBQXdDO0FBQ3BDLGNBQUUsRUFBRixFQUFNLElBQU4sQ0FBVyxnQkFBWCxFQUE2QixRQUE3QjtBQUNIOztBQUdELGlCQUFTLGVBQVQsR0FBMkI7QUFDdkIsZ0JBQUksWUFBWSxLQUFoQixFQUF1QjtBQUNuQixvQkFBSSxPQUFPLHNDQUFYO0FBQ0Esd0JBQVEsV0FBUjtBQUNBLHdCQUFRLFFBQVI7QUFDQSxrQkFBRSxnQkFBRixFQUFvQixNQUFwQixDQUEyQixJQUEzQjtBQUNBLDJCQUFXLElBQVg7O0FBRUEsa0JBQUUsWUFBRixFQUFnQixFQUFoQixDQUFtQixPQUFuQixFQUE0QixZQUFZO0FBQ3BDO0FBQ0gsaUJBRkQ7QUFHSDtBQUNKOztBQUdELGlCQUFTLGNBQVQsR0FBMEI7QUFDdEIsZ0JBQUksS0FBSyxFQUFFLE1BQUYsRUFBVSxJQUFWLENBQWUsWUFBZixDQUFUO0FBQ0EsZ0JBQUksV0FBVyxFQUFFLE1BQUYsRUFBVSxJQUFWLENBQWUsZ0JBQWYsQ0FBZjtBQUNBLGdCQUFJLGFBQWEsSUFBakIsRUFBdUI7QUFDbkIsMkJBQVcsQ0FBWDtBQUNIOztBQUVELGdCQUFLLFdBQVcsWUFBWSxFQUFaLEVBQWdCLEtBQWhCLENBQXNCLE1BQXRCLEdBQStCLENBQS9DLEVBQWtEO0FBQzlDLDJCQUFXLENBQVg7QUFDQSxrQkFBRSxNQUFGLEVBQVUsSUFBVixDQUFlLGdCQUFmLEVBQWlDLFFBQWpDO0FBQ0g7O0FBRUQsaUJBQUssSUFBSSxJQUFJLENBQWIsRUFBZ0IsSUFBSSxZQUFZLEVBQVosRUFBZ0IsS0FBaEIsQ0FBc0IsTUFBMUMsRUFBa0QsR0FBbEQsRUFBdUQ7O0FBRW5ELG9CQUFJLGVBQWUsRUFBRSxRQUFGLEVBQVksRUFBWixDQUFlLFFBQWYsQ0FBbkI7QUFDQSxrQkFBRSx1QkFBRixFQUEyQixJQUEzQixDQUFnQyxNQUFJLFlBQVksRUFBWixFQUFnQixLQUFoQixDQUFzQixRQUF0QixDQUFwQzs7QUFFQSxvQkFBRyxhQUFhLFFBQWIsQ0FBc0IsZUFBdEIsQ0FBSCxFQUEwQztBQUN2QyxzQkFBRSxzQkFBRixFQUEwQixJQUExQixDQUErQiwwQkFBL0I7QUFDQSxzQkFBRSw0QkFBRixFQUFnQyxJQUFoQyxDQUFxQyxVQUFyQyxFQUFpRCxFQUFqRCxFQUFxRCxHQUFyRCxDQUF5RCxDQUF6RDtBQUNBLHNCQUFFLHVCQUFGLEVBQTJCLElBQTNCLENBQWdDLFdBQWhDO0FBQ0Esc0JBQUUsTUFBRixFQUFVLElBQVYsQ0FBZSxtQkFBZixFQUFvQyxPQUFwQztBQUNGLGlCQUxELE1BS087QUFDSCxzQkFBRSxzQkFBRixFQUEwQixJQUExQixDQUErQixhQUEvQjtBQUNBLHNCQUFFLDRCQUFGLEVBQWdDLFVBQWhDLENBQTJDLFVBQTNDLEVBQXVELEdBQXZELENBQTJELENBQTNEO0FBQ0Esc0JBQUUsTUFBRixFQUFVLElBQVYsQ0FBZSxtQkFBZixFQUFvQyxNQUFwQztBQUNIO0FBQ0o7QUFDSjs7QUFHRCxpQkFBUyxlQUFULEdBQTJCO0FBQ3ZCO0FBQ0EsZ0JBQUksU0FBUyxFQUFiO0FBQ0EsaUJBQUssSUFBSSxJQUFJLENBQWIsRUFBZ0IsSUFBSSxJQUFJLE1BQXhCLEVBQWdDLEdBQWhDLEVBQXFDO0FBQ2pDLG9CQUFJLElBQUksU0FBUixFQUFrQjtBQUNkLHFDQUFpQixJQUFJLENBQUosQ0FBakI7QUFDSCxpQkFGRCxNQUVPO0FBQ0gsMkJBQU8sSUFBUCxDQUFZLElBQUksQ0FBSixDQUFaO0FBQ0g7QUFFSjtBQUNELGtCQUFNLE1BQU47QUFDQSxnQkFBSSxJQUFJLE1BQUosR0FBYSxDQUFqQixFQUFvQjtBQUNoQiwyQkFBVyxLQUFYO0FBQ0E7QUFDSDtBQUNKOztBQUdELFVBQUUsYUFBRixFQUFpQixFQUFqQixDQUFvQixPQUFwQixFQUE2QixVQUFVLEtBQVYsRUFBaUI7QUFBMEI7QUFDcEUsZ0JBQUksS0FBSyxNQUFNLE1BQWY7QUFDQSxvQ0FBd0IsV0FBeEIsRUFBcUMsRUFBRSxFQUFGLEVBQU0sSUFBTixDQUFXLGdCQUFYLENBQXJDO0FBQ0EsZ0JBQUksRUFBRSxFQUFGLEVBQU0sUUFBTixDQUFlLGtCQUFmLENBQUosRUFBdUM7QUFDbkMsb0JBQUksVUFBVSxFQUFFLEVBQUYsRUFBTSxJQUFOLENBQVcsZ0JBQVgsQ0FBZDtBQUNBLG9CQUFJLE9BQU8sRUFBRSxtQkFBRixFQUF1QixJQUF2QixDQUE0QixnQkFBNUIsQ0FBWDtBQUNBO0FBQ0Esb0JBQUcsVUFBVSxnQkFBYixFQUErQjtBQUMzQiw4QkFBVSxDQUFWO0FBQ0g7QUFDRDtBQUNBLG9CQUFJLE9BQU8sZ0JBQVgsRUFBNkI7QUFDekIsMkJBQU8sQ0FBUDtBQUNIO0FBQ0Qsa0JBQUUsRUFBRixFQUFNLElBQU4sQ0FBVyxnQkFBWCxFQUE2QixPQUE3QjtBQUNBLGtCQUFFLG1CQUFGLEVBQXVCLElBQXZCLENBQTRCLGdCQUE1QixFQUE4QyxJQUE5QztBQUdILGFBZkQsTUFlTyxJQUFJLEVBQUUsRUFBRixFQUFNLFFBQU4sQ0FBZSxrQkFBZixDQUFKLEVBQXVDOztBQUUxQyxvQkFBSSxXQUFVLEVBQUUsRUFBRixFQUFNLElBQU4sQ0FBVyxnQkFBWCxDQUFkO0FBQ0Esb0JBQUksT0FBTyxFQUFFLG1CQUFGLEVBQXVCLElBQXZCLENBQTRCLGdCQUE1QixDQUFYO0FBQ0E7QUFDQSxvQkFBRyxXQUFVLENBQWIsRUFBZ0I7QUFDWiwrQkFBVSxnQkFBVjtBQUNIO0FBQ0Q7QUFDQSxvQkFBSSxPQUFPLENBQVgsRUFBYTtBQUNULDJCQUFPLGdCQUFQO0FBQ0g7QUFDRCxrQkFBRSxFQUFGLEVBQU0sSUFBTixDQUFXLGdCQUFYLEVBQTZCLFFBQTdCO0FBQ0Esa0JBQUUsbUJBQUYsRUFBdUIsSUFBdkIsQ0FBNEIsZ0JBQTVCLEVBQThDLElBQTlDO0FBQ0g7QUFFSixTQWxDRDs7QUFxQ0EsVUFBRSx3QkFBRixFQUE0QixFQUE1QixDQUErQixPQUEvQixFQUF3QyxZQUFVOztBQUU5QyxjQUFFLHVCQUFGLEVBQTJCLElBQTNCLENBQWdDLElBQWhDO0FBQ0EsZ0JBQUksQ0FBQyxFQUFFLElBQUYsRUFBUSxPQUFSLENBQWdCLGlCQUFoQixFQUFtQyxRQUFuQyxDQUE0QyxXQUE1QyxDQUFMLEVBQStEO0FBQzNELGtCQUFFLGlCQUFGLEVBQXFCLFdBQXJCLENBQWlDLFdBQWpDO0FBQ0Esa0JBQUUsdUJBQUYsRUFBMkIsT0FBM0I7QUFDSDtBQUNELGNBQUUsSUFBRixFQUFRLE9BQVIsQ0FBZ0IsaUJBQWhCLEVBQW1DLFdBQW5DLENBQStDLFdBQS9DO0FBQ0EsY0FBRSxJQUFGLEVBQVEsT0FBUixDQUFnQixpQkFBaEIsRUFBbUMsSUFBbkMsQ0FBd0MsdUJBQXhDLEVBQWlFLFdBQWpFO0FBRUgsU0FWRDs7QUFhQSxZQUFHLEVBQUUsaUJBQUYsRUFBcUIsTUFBeEIsRUFBK0I7QUFDM0IsdUJBQVcsWUFBVTtBQUNqQixrQkFBRSxpQkFBRixFQUFxQixLQUFyQixHQUE2QixJQUE3QixDQUFrQyx3QkFBbEMsRUFBNEQsS0FBNUQ7QUFDSCxhQUZELEVBRUUsR0FGRjtBQUdIOztBQUdELFVBQUUsNEJBQUYsRUFBZ0MsRUFBaEMsQ0FBbUMsUUFBbkMsRUFBNkMsWUFBWTtBQUNyRCxnQkFBSSxNQUFNLEVBQUUsNEJBQUYsRUFBZ0MsR0FBaEMsRUFBVjtBQUNBLGdCQUFJLE1BQU0sRUFBVixFQUFjO0FBQ1Ysc0JBQU0sRUFBTjtBQUNBLGtCQUFFLDRCQUFGLEVBQWdDLEdBQWhDLENBQW9DLEdBQXBDO0FBQ0gsYUFIRCxNQUdPLElBQUksTUFBTSxDQUFWLEVBQWE7QUFDaEIsc0JBQU0sQ0FBTjtBQUNBLGtCQUFFLDRCQUFGLEVBQWdDLEdBQWhDLENBQW9DLEdBQXBDO0FBQ0g7QUFDSixTQVREOztBQVlBLFVBQUUsc0JBQUYsRUFBMEIsRUFBMUIsQ0FBNkIsT0FBN0IsRUFBcUMsWUFBVTtBQUMzQyxnQkFBSSxjQUFjLEVBQUUsTUFBRixFQUFVLElBQVYsQ0FBZSxtQkFBZixDQUFsQjtBQUNBLGdCQUFJLGVBQWUsTUFBbkIsRUFBMkI7QUFDdkIsb0JBQUksUUFBUSx5REFBd0QsRUFBRSx1QkFBRixFQUEyQixJQUEzQixFQUF4RCxHQUEyRiwwR0FBdkc7QUFDQSxrQkFBRSxNQUFGLEVBQVUsTUFBVixDQUFpQixLQUFqQjtBQUNBLGtCQUFFLFFBQUYsRUFBWSxJQUFaLEdBQW1CLE1BQW5CLENBQTBCLEdBQTFCO0FBQ0gsYUFKRCxNQUlPO0FBQ0gsb0JBQUksU0FBUSwrQ0FBOEMsRUFBRSx1QkFBRixFQUEyQixJQUEzQixFQUE5QyxHQUFpRiwwS0FBN0Y7QUFDQSxrQkFBRSxNQUFGLEVBQVUsTUFBVixDQUFpQixNQUFqQjtBQUNBLGtCQUFFLFFBQUYsRUFBWSxJQUFaLEdBQW1CLE1BQW5CLENBQTBCLEdBQTFCO0FBQ0g7O0FBRUQsY0FBRSxvQkFBRixFQUF3QixFQUF4QixDQUEyQixPQUEzQixFQUFvQyxZQUFVO0FBQ3pDLGtCQUFFLFFBQUYsRUFBWSxPQUFaLENBQW9CLEdBQXBCLEVBQXlCLE1BQXpCO0FBQ0osYUFGRDs7QUFJQSxjQUFFLE1BQUYsRUFBVSxFQUFWLENBQWEsT0FBYixFQUFzQixVQUFVLEtBQVYsRUFBaUI7QUFDbkMsb0JBQUksQ0FBQyxFQUFFLE1BQU0sTUFBUixFQUFnQixPQUFoQixDQUF3QixFQUFFLFFBQUYsRUFBWSxHQUFaLENBQWdCLHNCQUFoQixDQUF4QixFQUFpRSxNQUF0RSxFQUE4RTtBQUMxRSxzQkFBRSxRQUFGLEVBQVksT0FBWixDQUFvQixHQUFwQixFQUF5QixNQUF6QjtBQUNIO0FBQ0osYUFKRDtBQUtILFNBckJEO0FBdUJILEtBN2FEO0FBOGFILENBL2FELEVBK2FHLE1BL2FIIiwiZmlsZSI6ImdlbmVyYXRlZC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzQ29udGVudCI6WyIoZnVuY3Rpb24oKXtmdW5jdGlvbiByKGUsbix0KXtmdW5jdGlvbiBvKGksZil7aWYoIW5baV0pe2lmKCFlW2ldKXt2YXIgYz1cImZ1bmN0aW9uXCI9PXR5cGVvZiByZXF1aXJlJiZyZXF1aXJlO2lmKCFmJiZjKXJldHVybiBjKGksITApO2lmKHUpcmV0dXJuIHUoaSwhMCk7dmFyIGE9bmV3IEVycm9yKFwiQ2Fubm90IGZpbmQgbW9kdWxlICdcIitpK1wiJ1wiKTt0aHJvdyBhLmNvZGU9XCJNT0RVTEVfTk9UX0ZPVU5EXCIsYX12YXIgcD1uW2ldPXtleHBvcnRzOnt9fTtlW2ldWzBdLmNhbGwocC5leHBvcnRzLGZ1bmN0aW9uKHIpe3ZhciBuPWVbaV1bMV1bcl07cmV0dXJuIG8obnx8cil9LHAscC5leHBvcnRzLHIsZSxuLHQpfXJldHVybiBuW2ldLmV4cG9ydHN9Zm9yKHZhciB1PVwiZnVuY3Rpb25cIj09dHlwZW9mIHJlcXVpcmUmJnJlcXVpcmUsaT0wO2k8dC5sZW5ndGg7aSsrKW8odFtpXSk7cmV0dXJuIG99cmV0dXJuIHJ9KSgpIiwiKGZ1bmN0aW9uICgkKSB7XHJcbiAgICAkKGRvY3VtZW50KS5yZWFkeShmdW5jdGlvbiAoKSB7XHJcblxyXG4gICAgICAgIGxldCBudW1iZXJPZlByb2R1Y3RzO1xyXG4gICAgICAgIGxldCBsb2FkTGltaXQgPSA1O1xyXG4gICAgICAgIGxldCBsaW1pdEJ0biA9IGZhbHNlO1xyXG4gICAgICAgIGxldCBhcnIgPSBbXTtcclxuICAgICAgICBsZXQgcHJvZHVjdHNBcnIgPSBbXTtcclxuXHJcblxyXG4gICAgICAgICQuZ2V0SlNPTigncHJvZHVjdHMuanNvbicsIGZ1bmN0aW9uKGRhdGEpe1xyXG5cclxuICAgICAgICAgICAgaWYgKGRhdGEucHJvZHVjdHMpIHtcclxuICAgICAgICAgICAgICAgIGdldE51bWJlclByb2R1Y3RzKGRhdGEucHJvZHVjdHMpO1xyXG4gICAgICAgICAgICAgICAgaWYgKCQoJ2JvZHknKS5oYXNDbGFzcygnc2luZ2xlLXByb2R1Y3QtcGFnZScpKXtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgZm9yIChsZXQgaSA9IDA7IGkgPCBudW1iZXJPZlByb2R1Y3RzOyBpKyspIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgcHJvZHVjdHNBcnIucHVzaChkYXRhLnByb2R1Y3RzW2ldKTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgbG9hZENvbnRlbnRBYm91dFByb2R1Y3QoZGF0YS5wcm9kdWN0cywgbG9hZEluZm8oKSk7XHJcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgIGZvciAobGV0IGkgPSAwOyBpIDwgbnVtYmVyT2ZQcm9kdWN0czsgaSsrKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHByb2R1Y3RzQXJyLnB1c2goZGF0YS5wcm9kdWN0c1tpXSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmICgoaSA8IGxvYWRMaW1pdCkpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNyZWF0ZURPTWZvckdyaWQoZGF0YS5wcm9kdWN0c1tpXSk7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgcGFzdExvYWRNb3JlQnRuKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBhcnIucHVzaChkYXRhLnByb2R1Y3RzW2ldKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKCdFcnJvciEgSW5jb3JyZWN0IEpTT04gZmlsZS4nKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAkKCcuc2xpZGVyJykuc2xpY2soe1xyXG4gICAgICAgICAgICAgICAgc2xpZGVzVG9TaG93OiAxLFxyXG4gICAgICAgICAgICAgICAgc2xpZGVzVG9TY3JvbGw6IDFcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSk7XHJcblxyXG5cclxuICAgICAgICBmdW5jdGlvbiBnZXROdW1iZXJQcm9kdWN0cyhwcm9kdWN0cyl7XHJcbiAgICAgICAgICAgIG51bWJlck9mUHJvZHVjdHMgPSBwcm9kdWN0cy5sZW5ndGg7XHJcbiAgICAgICAgfVxyXG5cclxuXHJcbiAgICAgICAgZnVuY3Rpb24gbG9hZENvbnRlbnRBYm91dFByb2R1Y3QocHJvZHVjdHMsIGlkKXtcclxuICAgICAgICAgICAgbGV0IHByb2RJZCA9IGlkIC0gMTtcclxuICAgICAgICAgICAgaWYgKHByb2RJZCA9PT0gbnVsbCB8fCBwcm9kSWQgPCAwKSB7XHJcbiAgICAgICAgICAgICAgICBwcm9kSWQgPSAwO1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAkKCdib2R5JykuYXR0cigncHJvZHVjdC1pZCcsIHByb2RJZCk7XHJcblxyXG5cclxuICAgICAgICAgICAgJCgnLnNpbmdsZS1wcm9kdWN0LXRpdGxlJykudGV4dChwcm9kdWN0c1twcm9kSWRdLnRpdGxlKTtcclxuICAgICAgICAgICAgJCgnLnNpbmdsZS1wcm9kdWN0LXByaWNlJykudGV4dCgnJCcrcHJvZHVjdHNbcHJvZElkXS5wcmljZVswXSk7XHJcblxyXG5cclxuICAgICAgICAgICAgaWYgKHByb2R1Y3RzW3Byb2RJZF0uY29sb3IpIHtcclxuICAgICAgICAgICAgICAgbGV0IGh0bWwgPSAnJztcclxuICAgICAgICAgICAgICAgIGZvciAobGV0IGkgPSAwOyBpIDwgcHJvZHVjdHNbcHJvZElkXS5jb2xvci5sZW5ndGg7IGkrKykge1xyXG4gICAgICAgICAgICAgICAgICAgIGxldCBub3RBdkNvbG9yID0gZnVuY3Rpb24gKCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBsZXQgYXJyID0gcHJvZHVjdHNbcHJvZElkXS5hdmFsaWFibGVfY29sb3I7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmKCEoYXJyLmluZGV4T2YocHJvZHVjdHNbcHJvZElkXS5jb2xvcltpXSkgIT0gLTEpKXtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiAnbm90LWF2YWlsYWJsZSdcclxuICAgICAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiAnYXZhaWxhYmxlJ1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgfTtcclxuICAgICAgICAgICAgICAgICAgICBodG1sICs9ICc8ZGl2IGNsYXNzPVwiY29sb3IgJyArIG5vdEF2Q29sb3IoKSArICdcIiBzdHlsZT1cIndpZHRoOiA0MHB4OyBoZWlnaHQ6IDQwcHg7IGJhY2tncm91bmQtY29sb3I6ICcrIHByb2R1Y3RzW3Byb2RJZF0uY29sb3JbaV0gKyc7IGJhY2tncm91bmQtaW1hZ2U6IHVybCgnICsgJ2ltYWdlcy9tYXRlcmlhbHMvJyArIHByb2R1Y3RzW3Byb2RJZF0ubWF0ZXJpYWwgKyAnLScgKyBwcm9kdWN0c1twcm9kSWRdLmNvbG9yW2ldICsgJy5qcGcnKycpXCI+JztcclxuICAgICAgICAgICAgICAgICAgICBodG1sICs9ICc8ZGl2IGNsYXNzPVwicG9wdXAtY29sb3JcIj48c3Bhbj4nKyBwcm9kdWN0c1twcm9kSWRdLmNvbG9yW2ldICsnIC80MzQxPC9zcGFuPjxpbWcgYWx0PVwiY29sb3JcIiBzcmM9XCJpbWFnZXMvbWF0ZXJpYWxzLycrIHByb2R1Y3RzW3Byb2RJZF0ubWF0ZXJpYWwgKyAnLScgKyBwcm9kdWN0c1twcm9kSWRdLmNvbG9yW2ldICsgJy5qcGcnKydcIj48L2Rpdj4nO1xyXG4gICAgICAgICAgICAgICAgICAgIGh0bWwgKz0gJzwvZGl2Pic7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAkKCcuY29sb3JzJykuZW1wdHkoKS5hcHBlbmQoaHRtbCk7XHJcbiAgICAgICAgICAgICAgICAkKCcuY29sb3JzJykub2ZmKCdjbGljaycpO1xyXG4gICAgICAgICAgICAgICAgJCgnLmNvbG9yJykub24oJ2NsaWNrJywgZnVuY3Rpb24gKGV2ZW50KSB7XHJcbiAgICAgICAgICAgICAgICAgICAgY2hhbmdlQ29sb3Jmb3JTaW5nbGVQYWdlKGV2ZW50KTtcclxuICAgICAgICAgICAgICAgIH0pO1xyXG5cclxuICAgICAgICAgICAgICAgICQoJy5jb2xvcicpLm1vdXNlZW50ZXIoZnVuY3Rpb24gKCkge1xyXG4gICAgICAgICAgICAgICAgICAgICQodGhpcykuZmluZCgnLnBvcHVwLWNvbG9yJykuc3RvcCh0cnVlKS5mYWRlSW4oMzAwKTtcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgJCgnLmNvbG9yJykubW91c2VsZWF2ZShmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgJCh0aGlzKS5maW5kKCcucG9wdXAtY29sb3InKS5zdG9wKHRydWUpLmZhZGVPdXQoMzAwKTtcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICBsZXQgc2xpZGVySFRNTCA9Jyc7XHJcbiAgICAgICAgICAgIGlmIChwcm9kdWN0c1twcm9kSWRdLmltYWdlcykge1xyXG5cclxuICAgICAgICAgICAgICAgIGZvciAobGV0IGkgPSAwOyBpIDwgcHJvZHVjdHNbcHJvZElkXS5pbWFnZXMubGVuZ3RoOyBpKyspe1xyXG4gICAgICAgICAgICAgICAgICAgIHNsaWRlckhUTUwgKz0gJzxpbWcgc3JjPVwiaW1hZ2VzLycrIHByb2R1Y3RzW3Byb2RJZF0uc3JjICsnLycrIHByb2R1Y3RzW3Byb2RJZF0uaW1hZ2VzW2ldKyAnXCI+JztcclxuICAgICAgICAgICAgICAgIH1cclxuXHJcblxyXG4gICAgICAgICAgICAgICAgaWYgKCQoJy5tYWluLXNsaWRlcicpLmhhc0NsYXNzKCdzbGljay1pbml0aWFsaXplZCcpKXtcclxuICAgICAgICAgICAgICAgICAgICAkKCcubWFpbi1zbGlkZXInKS5zbGljaygndW5zbGljaycpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgaWYgKCQoJy5uYXYtc2xpZGVyJykuaGFzQ2xhc3MoJ3NsaWNrLWluaXRpYWxpemVkJykpe1xyXG4gICAgICAgICAgICAgICAgICAgICQoJy5uYXYtc2xpZGVyJykuc2xpY2soJ3Vuc2xpY2snKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICQoJy5tYWluLXNsaWRlcicpLmVtcHR5KCkuYXBwZW5kKHNsaWRlckhUTUwpO1xyXG4gICAgICAgICAgICAgICAgJCgnLm5hdi1zbGlkZXInKS5lbXB0eSgpLmFwcGVuZChzbGlkZXJIVE1MKTtcclxuXHJcbiAgICAgICAgICAgICAgICBpZiAoJCgnLm1haW4tc2xpZGVyJykuaGFzQ2xhc3MoJ3NsaWNrLWluaXRpYWxpemVkJykpe1xyXG4gICAgICAgICAgICAgICAgICAgICQoJy5tYWluLXNsaWRlcicpLnNsaWNrKCd1bnNsaWNrJyk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAkKCcubWFpbi1zbGlkZXInKS5zbGljayh7XHJcbiAgICAgICAgICAgICAgICAgICAgYXJyb3dzOiBmYWxzZSxcclxuICAgICAgICAgICAgICAgICAgICBkcmFnZ2FibGU6IGZhbHNlLFxyXG4gICAgICAgICAgICAgICAgICAgIGluZmluaXRlOiBmYWxzZVxyXG4gICAgICAgICAgICAgICAgfSk7XHJcblxyXG5cclxuICAgICAgICAgICAgICAgICQoJy5uYXYtc2xpZGVyJykuc2xpY2soe1xyXG4gICAgICAgICAgICAgICAgICAgIGluZmluaXRlOiBmYWxzZSxcclxuICAgICAgICAgICAgICAgICAgICBzbGlkZXNUb1Nob3c6IDQsXHJcbiAgICAgICAgICAgICAgICAgICAgc2xpZGVzVG9TY3JvbGw6IDEsXHJcbiAgICAgICAgICAgICAgICAgICAgYXNOYXZGb3I6ICcubWFpbi1zbGlkZXInLFxyXG4gICAgICAgICAgICAgICAgICAgIGZvY3VzT25TZWxlY3Q6IHRydWUsXHJcbiAgICAgICAgICAgICAgICAgICAgZHJhZ2dhYmxlOiBmYWxzZSxcclxuICAgICAgICAgICAgICAgICAgICBhcnJvd3M6IHRydWVcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICBjaGVja0F2YWxpYWJsZSgpO1xyXG4gICAgICAgIH1cclxuXHJcblxyXG4gICAgICAgIGZ1bmN0aW9uIGNyZWF0ZURPTWZvckdyaWQocHJvZHVjdHMpIHtcclxuXHJcbiAgICAgICAgICAgIGxldCBodG1sID0gJzxkaXYgY2xhc3M9XCJwcm9kdWN0LWNhcmRcIiBkYXRhLWlkPVwiJysgcHJvZHVjdHMuaWQgKydcIj48ZGl2IGNsYXNzPVwicHJvZHVjdC1jYXJkLWlubmVyXCI+JztcclxuICAgICAgICAgICAgaWYgKHByb2R1Y3RzLmltYWdlcykge1xyXG4gICAgICAgICAgICAgICAgaHRtbCArPSAnPGRpdiBjbGFzcz1cInByb2R1Y3QtY2FyZC1pbWFnZXNcIj48YSBjbGFzcz1cInByb2R1Y3QtY2FyZC1saW5rXCIgaHJlZj1cInByb2R1Y3QuaHRtbFwiPic7XHJcbiAgICAgICAgICAgICAgICBmb3IgKGxldCBpID0gMDsgaSA8IDI7IGkrKyl7XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKHByb2R1Y3RzLmltYWdlc1tpXSl7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGh0bWwgKz0gJzxpbWcgc3JjPVwiaW1hZ2VzLycrIHByb2R1Y3RzLnNyYyArJy8nKyBwcm9kdWN0cy5pbWFnZXNbaV0rICdcIj4nO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIGh0bWwgKz0gJzwvYT48L2Rpdj4nO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIGh0bWwgKz0gJzxkaXYgY2xhc3M9XCJwcm9kdWN0LWNhcmQtZm9vdGVyXCI+JztcclxuICAgICAgICAgICAgaHRtbCArPSAnPGRpdiBjbGFzcz1cIndyYXBwZXJcIj48YSBjbGFzcz1cInByb2R1Y3QtY2FyZC1saW5rXCIgaHJlZj1cInByb2R1Y3QuaHRtbFwiPjxoMyBjbGFzcz1cInByb2R1Y3QtY2FyZC10aXRsZVwiPicgKyBwcm9kdWN0cy50aXRsZSArICc8L2gzPjwvYT4nO1xyXG4gICAgICAgICAgICBodG1sICs9JzxkaXYgY2xhc3M9XCJwcmljZS1zbGlkZXItJysgcHJvZHVjdHMuaWQgKydcIj4nO1xyXG4gICAgICAgICAgICBmb3IgKGxldCBpID0gMDsgaSA8IHByb2R1Y3RzLnByaWNlLmxlbmd0aDsgaSsrKSB7XHJcbiAgICAgICAgICAgICAgICBodG1sICs9ICc8c3BhbiBjbGFzcz1cInByb2R1Y3QtY2FyZC1wcmljZSBhbmltYXRlZFwiPicrJyQnICsgcHJvZHVjdHMucHJpY2VbaV0gKyc8L3NwYW4+JztcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgaHRtbCArPSAnPC9kaXY+PC9kaXY+JztcclxuXHJcbiAgICAgICAgICAgIGlmIChwcm9kdWN0cy5jb2xvcikge1xyXG4gICAgICAgICAgICAgICAgaHRtbCArPSAnPGRpdiBjbGFzcz1cInByb2R1Y3QtY2FyZC1jb2xvcnMganMtc2xpZGVyLWNvbG9yLScgK3Byb2R1Y3RzLmlkKyAnXCI+JztcclxuICAgICAgICAgICAgICAgIGZvciAobGV0IGkgPSAwOyBpIDwgcHJvZHVjdHMuY29sb3IubGVuZ3RoOyBpKyspIHtcclxuICAgICAgICAgICAgICAgICAgICBsZXQgbm90QXZDb2xvciA9IGZ1bmN0aW9uICgpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgbGV0IGFyciA9IHByb2R1Y3RzLmF2YWxpYWJsZV9jb2xvcjtcclxuICAgICAgICAgICAgICAgICAgICAgICAgaWYoIShhcnIuaW5kZXhPZihwcm9kdWN0cy5jb2xvcltpXSkgIT0gLTEpKXtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiAnbm90LWF2YWlsYWJsZSdcclxuICAgICAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiAnYXZhaWxhYmxlJ1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgfTtcclxuICAgICAgICAgICAgICAgICAgICBodG1sICs9ICc8ZGl2IGNsYXNzPVwicHJvZHVjdC1jYXJkX19jb2xvciAnICsgbm90QXZDb2xvcigpICsgJ1wiIHN0eWxlPVwid2lkdGg6IDQwcHg7IGhlaWdodDogNDBweDsgYmFja2dyb3VuZC1jb2xvcjogJysgcHJvZHVjdHMuY29sb3JbaV0gKyc7IGJhY2tncm91bmQtaW1hZ2U6IHVybCgnICsgJ2ltYWdlcy9tYXRlcmlhbHMvJyArIHByb2R1Y3RzLm1hdGVyaWFsICsgJy0nICsgcHJvZHVjdHMuY29sb3JbaV0gKyAnLmpwZycrJylcIiBcIj48L2Rpdj4nO1xyXG5cclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIGh0bWwgKz0gJzwvZGl2Pic7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgaHRtbCArPSAnPC9kaXY+JztcclxuICAgICAgICAgICAgaHRtbCArPSAnPC9kaXY+PC9kaXY+JztcclxuXHJcbiAgICAgICAgICAgIGxldCAkbGFzdEVsID0gJCgnLnByb2R1Y3QtY2FyZCcpLmxhc3QoKTtcclxuXHJcbiAgICAgICAgICAgIGxldCBzbGlkZXJDbGFzcyA9ICcucHJpY2Utc2xpZGVyLScgKyBwcm9kdWN0cy5pZDtcclxuICAgICAgICAgICAgbGV0IHNsaWRlckNvbG9yQ2xhc3MgPSAnLmpzLXNsaWRlci1jb2xvci0nICsgcHJvZHVjdHMuaWQ7XHJcblxyXG4gICAgICAgICAgICAkKCcucHJvZHVjdHMtZ3JpZCcpLmFwcGVuZChodG1sKTtcclxuICAgICAgICAgICAgJChzbGlkZXJDbGFzcykuc2xpY2soe1xyXG4gICAgICAgICAgICAgICAgc2xpZGVzVG9TaG93OiAxLFxyXG4gICAgICAgICAgICAgICAgc2xpZGVzVG9TY3JvbGw6IDEsXHJcbiAgICAgICAgICAgICAgICBkcmFnZ2FibGU6IGZhbHNlLFxyXG4gICAgICAgICAgICAgICAgc3dpcGU6IGZhbHNlXHJcbiAgICAgICAgICAgIH0pO1xyXG5cclxuICAgICAgICAgICAgJChzbGlkZXJDb2xvckNsYXNzKS5zbGljayh7XHJcbiAgICAgICAgICAgICAgICBzbGlkZXNUb1Nob3c6IDIsXHJcbiAgICAgICAgICAgICAgICBzbGlkZXNUb1Njcm9sbDogMSxcclxuICAgICAgICAgICAgICAgIGFzTmF2Rm9yOiBzbGlkZXJDbGFzcyxcclxuICAgICAgICAgICAgICAgIGZvY3VzT25TZWxlY3Q6IHRydWVcclxuICAgICAgICAgICAgfSk7XHJcblxyXG4gICAgICAgICAgICAkKCcucHJvZHVjdC1jYXJkLWxpbmsnKS51bmJpbmQoJ2NsaWNrJyk7XHJcblxyXG4gICAgICAgICAgICAkKCcucHJvZHVjdC1jYXJkLWxpbmsnKS5vbignY2xpY2snLCBmdW5jdGlvbihldmVudCl7XHJcbiAgICAgICAgICAgICAgICBzYXZlSW5mbyhldmVudCk7XHJcbiAgICAgICAgICAgIH0pO1xyXG5cclxuICAgICAgICAgICAgJGxhc3RFbC5maW5kKCcucHJvZHVjdC1jYXJkX19jb2xvcicpLm9uKCdjbGljaycsIGZ1bmN0aW9uKGV2ZW50KXtcclxuICAgICAgICAgICAgICAgIGNoYW5nZUNvbG9yKGV2ZW50KVxyXG4gICAgICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgfVxyXG5cclxuXHJcbiAgICAgICAgZnVuY3Rpb24gY2hhbmdlQ29sb3IoZXZlbnQpIHtcclxuXHJcbiAgICAgICAgICAgIGxldCBlbGVtZW50ID0gZXZlbnQudGFyZ2V0LmNsb3Nlc3QoJy5wcm9kdWN0LWNhcmQnKTtcclxuICAgICAgICAgICAgbGV0IHByb2R1Y3RJZCA9IGVsZW1lbnQuZ2V0QXR0cmlidXRlKCdkYXRhLWlkJyk7XHJcbiAgICAgICAgICAgIGxldCBjb2xvclBvc2l0aW9uID0gJChldmVudC50YXJnZXQpLmluZGV4KCk7XHJcbiAgICAgICAgICAgIGNoYW5nZUNvbnRlbnQocHJvZHVjdElkLCBjb2xvclBvc2l0aW9uLCBlbGVtZW50KTtcclxuICAgICAgICB9XHJcblxyXG5cclxuICAgICAgICBmdW5jdGlvbiBjaGFuZ2VDb2xvcmZvclNpbmdsZVBhZ2UoZXZlbnQpIHtcclxuICAgICAgICAgICAgbGV0IGNvbG9yUG9zaXRpb24gPSAkKGV2ZW50LnRhcmdldCkuaW5kZXgoKTtcclxuICAgICAgICAgICAgaWYgKGNvbG9yUG9zaXRpb24gPT09IG51bGwpIHtcclxuICAgICAgICAgICAgICAgIGNvbG9yUG9zaXRpb24gPSAwO1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAkKCdib2R5JykuYXR0cignY29sb3ItcG9zaXRpb24nLCBjb2xvclBvc2l0aW9uKTtcclxuICAgICAgICAgICAgY2hlY2tBdmFsaWFibGUoKTtcclxuICAgICAgICB9XHJcblxyXG5cclxuICAgICAgICBmdW5jdGlvbiByZW1vdmVMb2FkTW9yZUJ0bigpIHtcclxuICAgICAgICAgICAgJCgnLmxvYWQtbW9yZScpLnJlbW92ZSgpO1xyXG4gICAgICAgIH1cclxuXHJcblxyXG4gICAgICAgIGZ1bmN0aW9uIHNhdmVJbmZvKCkge1xyXG4gICAgICAgICAgICBsZXQgZWxlbWVudCA9IGV2ZW50LnRhcmdldC5jbG9zZXN0KCcucHJvZHVjdC1jYXJkJyk7XHJcbiAgICAgICAgICAgIGxldCBwcm9kdWN0SWQgPSBlbGVtZW50LmdldEF0dHJpYnV0ZSgnZGF0YS1pZCcpO1xyXG4gICAgICAgICAgICBpZiAoZWxlbWVudC5oYXNBdHRyaWJ1dGUoJ2NvbG9yLXBvc2l0aW9uJykpe1xyXG4gICAgICAgICAgICAgICAgbGV0IGNvbG9yUG9zID0gZWxlbWVudC5nZXRBdHRyaWJ1dGUoJ2NvbG9yLXBvc2l0aW9uJyk7XHJcbiAgICAgICAgICAgICAgICBsb2NhbFN0b3JhZ2Uuc2V0SXRlbSgnY29sb3JQb3MnLCBjb2xvclBvcyk7XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICBsb2NhbFN0b3JhZ2Uuc2V0SXRlbSgnY29sb3JQb3MnLCAwKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBsb2NhbFN0b3JhZ2Uuc2V0SXRlbSgncHJvZElkJywgcHJvZHVjdElkKTtcclxuICAgICAgICB9XHJcblxyXG5cclxuICAgICAgICBmdW5jdGlvbiBsb2FkSW5mbygpIHtcclxuICAgICAgICAgICAgbGV0IHByb2RJZCA9IGxvY2FsU3RvcmFnZS5nZXRJdGVtKCdwcm9kSWQnKTtcclxuICAgICAgICAgICAgbGV0IGNvbG9yUG9zID0gbG9jYWxTdG9yYWdlLmdldEl0ZW0oJ2NvbG9yUG9zJyk7XHJcbiAgICAgICAgICAgIGlmIChwcm9kSWQgPT09IG51bGwpIHtcclxuICAgICAgICAgICAgICAgIHByb2RJZCA9IDA7XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIGlmIChjb2xvclBvcyA9PT0gbnVsbCkge1xyXG4gICAgICAgICAgICAgICAgY29sb3JQb3MgPSAwO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICQoJy5uZXh0LXByb2R1Y3QtYnRuJykuYXR0cignZGF0YS1zd2l0Y2gtaWQnLCBwYXJzZUludChwcm9kSWQpICsgMSk7XHJcbiAgICAgICAgICAgICQoJ2JvZHknKS5hdHRyKCdjb2xvci1wb3NpdGlvbicsIGNvbG9yUG9zKTtcclxuICAgICAgICAgICAgaWYgKHByb2RJZCA9PT0gMCB8fCBwcm9kSWQgPCAwKSB7XHJcbiAgICAgICAgICAgICAgICAkKCcucHJldi1wcm9kdWN0LWJ0bicpLmF0dHIoJ2RhdGEtc3dpdGNoLWlkJywgZnVuY3Rpb24gKCkge1xyXG4gICAgICAgICAgICAgICAgICAgIGxldCBwcmV2ID0gbnVtYmVyT2ZQcm9kdWN0cztcclxuICAgICAgICAgICAgICAgICAgICByZXR1cm4gcHJldlxyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAkKCcucHJldi1wcm9kdWN0LWJ0bicpLmF0dHIoJ2RhdGEtc3dpdGNoLWlkJywgcHJvZElkIC0gMSlcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBsb2NhbFN0b3JhZ2UuY2xlYXIoKTtcclxuXHJcbiAgICAgICAgICAgIHJldHVybiBwcm9kSWRcclxuICAgICAgICB9XHJcblxyXG5cclxuICAgICAgICBmdW5jdGlvbiBjaGFuZ2VDb250ZW50KGlkLCBwb3NpdGlvbiwgZWwpe1xyXG4gICAgICAgICAgICAkKGVsKS5hdHRyKCdjb2xvci1wb3NpdGlvbicsIHBvc2l0aW9uKTtcclxuICAgICAgICB9XHJcblxyXG5cclxuICAgICAgICBmdW5jdGlvbiBwYXN0TG9hZE1vcmVCdG4oKSB7XHJcbiAgICAgICAgICAgIGlmIChsaW1pdEJ0biA9PSBmYWxzZSkge1xyXG4gICAgICAgICAgICAgICAgbGV0IGh0bWwgPSAnPGRpdiBjbGFzcz1cInByb2R1Y3QtY2FyZCBsb2FkLW1vcmVcIj4nO1xyXG4gICAgICAgICAgICAgICAgaHRtbCArPSAnTG9hZCBtb3JlJztcclxuICAgICAgICAgICAgICAgIGh0bWwgKz0gJzwvZGl2Pic7XHJcbiAgICAgICAgICAgICAgICAkKCcucHJvZHVjdHMtZ3JpZCcpLmFwcGVuZChodG1sKTtcclxuICAgICAgICAgICAgICAgIGxpbWl0QnRuID0gdHJ1ZTtcclxuICAgICAgICAgICAgICAgIFxyXG4gICAgICAgICAgICAgICAgJCgnLmxvYWQtbW9yZScpLm9uKCdjbGljaycsIGZ1bmN0aW9uICgpIHtcclxuICAgICAgICAgICAgICAgICAgICBsb2FkTmV4dENvbnRlbnQoKTtcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG5cclxuXHJcbiAgICAgICAgZnVuY3Rpb24gY2hlY2tBdmFsaWFibGUoKSB7XHJcbiAgICAgICAgICAgIGxldCBpZCA9ICQoJ2JvZHknKS5hdHRyKCdwcm9kdWN0LWlkJyk7XHJcbiAgICAgICAgICAgIGxldCBjb2xvclBvcyA9ICQoJ2JvZHknKS5hdHRyKCdjb2xvci1wb3NpdGlvbicpO1xyXG4gICAgICAgICAgICBpZiAoY29sb3JQb3MgPT09IG51bGwpIHtcclxuICAgICAgICAgICAgICAgIGNvbG9yUG9zID0gMDtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgaWYgIChjb2xvclBvcyA+IHByb2R1Y3RzQXJyW2lkXS5jb2xvci5sZW5ndGggLSAxKSB7XHJcbiAgICAgICAgICAgICAgICBjb2xvclBvcyA9IDA7XHJcbiAgICAgICAgICAgICAgICAkKCdib2R5JykuYXR0cignY29sb3ItcG9zaXRpb24nLCBjb2xvclBvcyk7XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIGZvciAobGV0IGkgPSAwOyBpIDwgcHJvZHVjdHNBcnJbaWRdLmNvbG9yLmxlbmd0aDsgaSsrKSB7XHJcblxyXG4gICAgICAgICAgICAgICAgbGV0IGN1cnJlbnRDb2xvciA9ICQoJy5jb2xvcicpLmVxKGNvbG9yUG9zKTtcclxuICAgICAgICAgICAgICAgICQoJy5zaW5nbGUtcHJvZHVjdC1wcmljZScpLnRleHQoJyQnK3Byb2R1Y3RzQXJyW2lkXS5wcmljZVtjb2xvclBvc10pO1xyXG5cclxuICAgICAgICAgICAgICAgIGlmKGN1cnJlbnRDb2xvci5oYXNDbGFzcygnbm90LWF2YWlsYWJsZScpKXtcclxuICAgICAgICAgICAgICAgICAgICQoJy5zaW5nbGUtcHJvZHVjdC1jYXJ0JykudGV4dCgnTk9USUZZIE1FIFdIRU4gQVZBSUxBQkxFJyk7XHJcbiAgICAgICAgICAgICAgICAgICAkKCcuc2luZ2xlLXByb2R1Y3QtcXR5X19maWVsZCcpLmF0dHIoJ2Rpc2FibGVkJywgJycpLnZhbCgwKTtcclxuICAgICAgICAgICAgICAgICAgICQoJy5zaW5nbGUtcHJvZHVjdC1wcmljZScpLnRleHQoJ1dhaXQgbGlzdCcpO1xyXG4gICAgICAgICAgICAgICAgICAgJCgnYm9keScpLmF0dHIoJ3Byb2R1Y3QtYXZhaWxhYmxlJywgJ2ZhbHNlJyk7XHJcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgICQoJy5zaW5nbGUtcHJvZHVjdC1jYXJ0JykudGV4dCgnQWRkIHRvIGNhcnQnKTtcclxuICAgICAgICAgICAgICAgICAgICAkKCcuc2luZ2xlLXByb2R1Y3QtcXR5X19maWVsZCcpLnJlbW92ZUF0dHIoJ2Rpc2FibGVkJykudmFsKDEpO1xyXG4gICAgICAgICAgICAgICAgICAgICQoJ2JvZHknKS5hdHRyKCdwcm9kdWN0LWF2YWlsYWJsZScsICd0cnVlJyk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcblxyXG5cclxuICAgICAgICBmdW5jdGlvbiBsb2FkTmV4dENvbnRlbnQoKSB7XHJcbiAgICAgICAgICAgIHJlbW92ZUxvYWRNb3JlQnRuKCk7XHJcbiAgICAgICAgICAgIGxldCBhZGRBcnIgPSBbXTtcclxuICAgICAgICAgICAgZm9yIChsZXQgaSA9IDA7IGkgPCBhcnIubGVuZ3RoOyBpKyspIHtcclxuICAgICAgICAgICAgICAgIGlmIChpIDwgbG9hZExpbWl0KXtcclxuICAgICAgICAgICAgICAgICAgICBjcmVhdGVET01mb3JHcmlkKGFycltpXSk7XHJcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgIGFkZEFyci5wdXNoKGFycltpXSk7XHJcbiAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIGFyciA9IGFkZEFycjtcclxuICAgICAgICAgICAgaWYgKGFyci5sZW5ndGggPiAwKSB7XHJcbiAgICAgICAgICAgICAgICBsaW1pdEJ0biA9IGZhbHNlO1xyXG4gICAgICAgICAgICAgICAgcGFzdExvYWRNb3JlQnRuKCk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcblxyXG5cclxuICAgICAgICAkKCcuYnRuLXN3aXRjaCcpLm9uKCdjbGljaycsIGZ1bmN0aW9uIChldmVudCkgeyAgICAgICAgICAgICAgICAgICAgICAgICAvLyBOZXh0L3ByZXYgcHJvZHVjdCBzd2l0Y2hlclxyXG4gICAgICAgICAgICBsZXQgZWwgPSBldmVudC50YXJnZXQ7XHJcbiAgICAgICAgICAgIGxvYWRDb250ZW50QWJvdXRQcm9kdWN0KHByb2R1Y3RzQXJyLCAkKGVsKS5hdHRyKCdkYXRhLXN3aXRjaC1pZCcpKTtcclxuICAgICAgICAgICAgaWYgKCQoZWwpLmhhc0NsYXNzKCduZXh0LXByb2R1Y3QtYnRuJykpe1xyXG4gICAgICAgICAgICAgICAgbGV0IGN1cnJlbnQgPSAkKGVsKS5hdHRyKCdkYXRhLXN3aXRjaC1pZCcpO1xyXG4gICAgICAgICAgICAgICAgbGV0IHByZXYgPSAkKCcucHJldi1wcm9kdWN0LWJ0bicpLmF0dHIoJ2RhdGEtc3dpdGNoLWlkJyk7XHJcbiAgICAgICAgICAgICAgICBjdXJyZW50Kys7XHJcbiAgICAgICAgICAgICAgICBpZihjdXJyZW50ID4gbnVtYmVyT2ZQcm9kdWN0cykge1xyXG4gICAgICAgICAgICAgICAgICAgIGN1cnJlbnQgPSAwO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgcHJldisrO1xyXG4gICAgICAgICAgICAgICAgaWYgKHByZXYgPiBudW1iZXJPZlByb2R1Y3RzKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgcHJldiA9IDA7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAkKGVsKS5hdHRyKCdkYXRhLXN3aXRjaC1pZCcsIGN1cnJlbnQpO1xyXG4gICAgICAgICAgICAgICAgJCgnLnByZXYtcHJvZHVjdC1idG4nKS5hdHRyKCdkYXRhLXN3aXRjaC1pZCcsIHByZXYpO1xyXG5cclxuXHJcbiAgICAgICAgICAgIH0gZWxzZSBpZiAoJChlbCkuaGFzQ2xhc3MoJ3ByZXYtcHJvZHVjdC1idG4nKSl7XHJcblxyXG4gICAgICAgICAgICAgICAgbGV0IGN1cnJlbnQgPSAkKGVsKS5hdHRyKCdkYXRhLXN3aXRjaC1pZCcpO1xyXG4gICAgICAgICAgICAgICAgbGV0IG5leHQgPSAkKCcubmV4dC1wcm9kdWN0LWJ0bicpLmF0dHIoJ2RhdGEtc3dpdGNoLWlkJyk7XHJcbiAgICAgICAgICAgICAgICBjdXJyZW50LS07XHJcbiAgICAgICAgICAgICAgICBpZihjdXJyZW50IDwgMCkge1xyXG4gICAgICAgICAgICAgICAgICAgIGN1cnJlbnQgPSBudW1iZXJPZlByb2R1Y3RzO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgbmV4dC0tO1xyXG4gICAgICAgICAgICAgICAgaWYgKG5leHQgPCAwKXtcclxuICAgICAgICAgICAgICAgICAgICBuZXh0ID0gbnVtYmVyT2ZQcm9kdWN0cztcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICQoZWwpLmF0dHIoJ2RhdGEtc3dpdGNoLWlkJywgY3VycmVudCk7XHJcbiAgICAgICAgICAgICAgICAkKCcubmV4dC1wcm9kdWN0LWJ0bicpLmF0dHIoJ2RhdGEtc3dpdGNoLWlkJywgbmV4dCk7XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgfSk7XHJcblxyXG5cclxuICAgICAgICAkKCcuYWNjb3JkaW9uLWl0ZW1fX3RpdGxlJykub24oJ2NsaWNrJywgZnVuY3Rpb24oKXtcclxuXHJcbiAgICAgICAgICAgICQoJy5hY2NvcmRpb24taXRlbV9fYm9keScpLnN0b3AodHJ1ZSk7XHJcbiAgICAgICAgICAgIGlmICghJCh0aGlzKS5jbG9zZXN0KCcuYWNjb3JkaW9uLWl0ZW0nKS5oYXNDbGFzcygnaXMtYWN0aXZlJykpIHtcclxuICAgICAgICAgICAgICAgICQoJy5hY2NvcmRpb24taXRlbScpLnJlbW92ZUNsYXNzKCdpcy1hY3RpdmUnKTtcclxuICAgICAgICAgICAgICAgICQoJy5hY2NvcmRpb24taXRlbV9fYm9keScpLnNsaWRlVXAoKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAkKHRoaXMpLmNsb3Nlc3QoJy5hY2NvcmRpb24taXRlbScpLnRvZ2dsZUNsYXNzKCdpcy1hY3RpdmUnKTtcclxuICAgICAgICAgICAgJCh0aGlzKS5jbG9zZXN0KCcuYWNjb3JkaW9uLWl0ZW0nKS5maW5kKCcuYWNjb3JkaW9uLWl0ZW1fX2JvZHknKS5zbGlkZVRvZ2dsZSgpO1xyXG5cclxuICAgICAgICB9KTtcclxuXHJcblxyXG4gICAgICAgIGlmKCQoJy5hY2NvcmRpb24taXRlbScpLmxlbmd0aCl7XHJcbiAgICAgICAgICAgIHNldFRpbWVvdXQoZnVuY3Rpb24oKXtcclxuICAgICAgICAgICAgICAgICQoJy5hY2NvcmRpb24taXRlbScpLmZpcnN0KCkuZmluZCgnLmFjY29yZGlvbi1pdGVtX190aXRsZScpLmNsaWNrKCk7XHJcbiAgICAgICAgICAgIH0sMTAwKTtcclxuICAgICAgICB9XHJcblxyXG5cclxuICAgICAgICAkKCcuc2luZ2xlLXByb2R1Y3QtcXR5X19maWVsZCcpLm9uKCdjaGFuZ2UnLCBmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgICAgIGxldCB2YWwgPSAkKCcuc2luZ2xlLXByb2R1Y3QtcXR5X19maWVsZCcpLnZhbCgpO1xyXG4gICAgICAgICAgICBpZiAodmFsID4gOTkpIHtcclxuICAgICAgICAgICAgICAgIHZhbCA9IDk5O1xyXG4gICAgICAgICAgICAgICAgJCgnLnNpbmdsZS1wcm9kdWN0LXF0eV9fZmllbGQnKS52YWwodmFsKTtcclxuICAgICAgICAgICAgfSBlbHNlIGlmICh2YWwgPCAxKSB7XHJcbiAgICAgICAgICAgICAgICB2YWwgPSAxO1xyXG4gICAgICAgICAgICAgICAgJCgnLnNpbmdsZS1wcm9kdWN0LXF0eV9fZmllbGQnKS52YWwodmFsKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0pO1xyXG5cclxuXHJcbiAgICAgICAgJCgnLnNpbmdsZS1wcm9kdWN0LWNhcnQnKS5vbignY2xpY2snLGZ1bmN0aW9uKCl7XHJcbiAgICAgICAgICAgIGxldCBjdXJyZW50QXR0ciA9ICQoJ2JvZHknKS5hdHRyKCdwcm9kdWN0LWF2YWlsYWJsZScpO1xyXG4gICAgICAgICAgICBpZiAoY3VycmVudEF0dHIgPT0gJ3RydWUnKSB7XHJcbiAgICAgICAgICAgICAgICBsZXQgcG9wdXAgPSAnPGRpdiBjbGFzcz1cInBvcHVwXCI+PHVsPjxsaT5Db25ncmF0dWxhdGlvbnMhPC9saT48bGk+JysgJCgnLnNpbmdsZS1wcm9kdWN0LXRpdGxlJykudGV4dCgpICsnPC9saT48bGk+d2FzIGFkZGVkPC9saT48bGk+dG8geW91ciBjYXJ0ITwvbGk+PC91bD48YnV0dG9uIGNsYXNzPVwiY2FydC1jbG9zZS1idXR0b25cIj5DbG9zZTwvYnV0dG9uPjwvZGl2Pic7XHJcbiAgICAgICAgICAgICAgICAkKCdib2R5JykuYXBwZW5kKHBvcHVwKTtcclxuICAgICAgICAgICAgICAgICQoJy5wb3B1cCcpLmhpZGUoKS5mYWRlSW4oMzAwKTtcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIGxldCBwb3B1cCA9ICc8ZGl2IGNsYXNzPVwicG9wdXBcIj48dWw+PGxpPlNvcnJ5ITwvbGk+PGxpPicrICQoJy5zaW5nbGUtcHJvZHVjdC10aXRsZScpLnRleHQoKSArJzwvbGk+PGxpPndpdGggdGhpcyBvcHRpb25zPC9saT48bGk+aXMgbm90IGF2YWlsYWJsZSBub3cuPC9saT48bGk+V2Ugbm90aWZ5IHlvdXIgd2hlbiBpdCBhdmFpbGFibGUgYWdhaW4uPC9saT48L3VsPjxidXR0b24gY2xhc3M9XCJjYXJ0LWNsb3NlLWJ1dHRvblwiPkNsb3NlPC9idXR0b24+PC9kaXY+JztcclxuICAgICAgICAgICAgICAgICQoJ2JvZHknKS5hcHBlbmQocG9wdXApO1xyXG4gICAgICAgICAgICAgICAgJCgnLnBvcHVwJykuaGlkZSgpLmZhZGVJbigzMDApO1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAkKCcuY2FydC1jbG9zZS1idXR0b24nKS5vbignY2xpY2snLCBmdW5jdGlvbigpe1xyXG4gICAgICAgICAgICAgICAgICQoJy5wb3B1cCcpLmZhZGVPdXQoMzAwKS5yZW1vdmUoKTtcclxuICAgICAgICAgICAgfSk7XHJcblxyXG4gICAgICAgICAgICAkKCdib2R5Jykub24oJ2NsaWNrJywgZnVuY3Rpb24gKGV2ZW50KSB7XHJcbiAgICAgICAgICAgICAgICBpZiAoISQoZXZlbnQudGFyZ2V0KS5jbG9zZXN0KCQoJy5wb3B1cCcpLmFkZCgnLnNpbmdsZS1wcm9kdWN0LWNhcnQnKSkubGVuZ3RoKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgJCgnLnBvcHVwJykuZmFkZU91dCgzMDApLnJlbW92ZSgpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9KVxyXG4gICAgICAgIH0pO1xyXG5cclxuICAgIH0pO1xyXG59KShqUXVlcnkpOyJdfQ==
